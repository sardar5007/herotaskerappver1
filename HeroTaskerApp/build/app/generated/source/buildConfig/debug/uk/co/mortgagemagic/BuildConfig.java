/**
 * Automatically generated file. DO NOT MODIFY
 */
package uk.co.mortgagemagic;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "uk.co.mortgagemagic";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 13;
  public static final String VERSION_NAME = "13.0";
}
