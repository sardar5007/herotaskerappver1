import 'MediaUploadFilesModel.dart';

class MediaUploadFilesAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  dynamic messages;
  _ResponseData responseData;

  MediaUploadFilesAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory MediaUploadFilesAPIModel.fromJson(Map<String, dynamic> j) {
    return MediaUploadFilesAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']) ?? null,
      messages: j['Messages'],
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  List<dynamic> upload_pictures;
  _ErrorMessages({this.upload_pictures});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      upload_pictures: j['upload_pictures'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'upload_pictures': upload_pictures,
      };
}

class _ResponseData {
  List<dynamic> images;
  _ResponseData({this.images});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var listImages = [];
    try {
      listImages =
          j['Images'].map((i) => MediaUploadFilesModel.fromJson(i)).toList() ??
              [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(images: listImages ?? []);
  }
  Map<String, dynamic> toMap() => {
        'Images': images,
      };
}
