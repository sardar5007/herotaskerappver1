class CommonAPIModel {
  bool success;
  dynamic responseData;
  CommonAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'] ?? true;
    responseData = json['ResponseData'] ?? null;
  }
}
