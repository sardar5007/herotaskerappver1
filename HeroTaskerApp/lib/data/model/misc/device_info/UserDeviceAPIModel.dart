class UserDeviceAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  UserDeviceAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  UserDeviceAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserDeviceModel userDevice;
  ResponseData({this.userDevice});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userDevice = json['UserDevice'] != null
        ? new UserDeviceModel.fromJson(json['UserDevice'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userDevice != null) {
      data['UserDevice'] = this.userDevice.toJson();
    }
    return data;
  }
}

class UserDeviceModel {
  int userId;
  dynamic user;
  int status;
  String creationDate;
  String eventType;
  String deviceName;
  String modleName;
  String oSVersion;
  String eventDate;
  String remarks;
  String profileImageUrl;
  String profileOwnerName;
  String deviceId;
  int id;

  UserDeviceModel(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.eventType,
      this.deviceName,
      this.modleName,
      this.oSVersion,
      this.eventDate,
      this.remarks,
      this.profileImageUrl,
      this.profileOwnerName,
      this.deviceId,
      this.id});

  UserDeviceModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] ?? {};
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    eventType = json['EventType'] ?? '';
    deviceName = json['DeviceName'] ?? '';
    modleName = json['ModleName'] ?? '';
    oSVersion = json['OSVersion'] ?? '';
    eventDate = json['EventDate'] ?? '';
    remarks = json['Remarks'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    profileOwnerName = json['ProfileOwnerName'] ?? '';
    deviceId = json['DeviceId'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['EventType'] = this.eventType;
    data['DeviceName'] = this.deviceName;
    data['ModleName'] = this.modleName;
    data['OSVersion'] = this.oSVersion;
    data['EventDate'] = this.eventDate;
    data['Remarks'] = this.remarks;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['DeviceId'] = this.deviceId;
    data['Id'] = this.id;
    return data;
  }
}
