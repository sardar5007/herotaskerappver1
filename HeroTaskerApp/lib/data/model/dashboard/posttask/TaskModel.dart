class TaskImages {
  List<Map<String, dynamic>> listImages;
}

class TaskModel {
  String creationDate;
  String deliveryDate;
  String deliveryTime;
  String description;
  double dueAmount;
  int dutDateType;
  int employeeId;
  double fixedBudgetAmount;
  double hourlyRate;
  int id;
  String imageServerUrl;
  bool isFixedPrice;
  bool isInPersonOrOnline;
  bool isReadMessage;
  String jobCategory;
  double latitude;
  double longitude;
  double netTotalAmount;
  double paidAmount;
  String ownerImageUrl;
  String ownerName;
  String remarks;
  String preferedLocation;
  String requirements;
  String skill;
  dynamic status;
  String taskTitleUrl;
  String title;
  String thumbnailPath;
  int totalAcceptedNumber;
  int totalBidsNumber;
  int totalCompletedNumber;
  dynamic totalHours;
  String updatedDate;
  int userId;
  int workerNumber;

  TaskModel(
      {this.creationDate,
      this.deliveryDate,
      this.deliveryTime,
      this.description,
      this.dueAmount,
      this.dutDateType,
      this.employeeId,
      this.fixedBudgetAmount,
      this.hourlyRate,
      this.id,
      this.imageServerUrl,
      this.isFixedPrice,
      this.isInPersonOrOnline,
      this.isReadMessage,
      this.jobCategory,
      this.latitude,
      this.longitude,
      this.netTotalAmount,
      this.paidAmount,
      this.ownerImageUrl,
      this.ownerName,
      this.remarks,
      this.preferedLocation,
      this.requirements,
      this.skill,
      this.status,
      this.taskTitleUrl,
      this.title,
      this.thumbnailPath,
      this.totalAcceptedNumber,
      this.totalBidsNumber,
      this.totalCompletedNumber,
      this.totalHours,
      this.updatedDate,
      this.userId,
      this.workerNumber});

  TaskModel.fromJson(Map<String, dynamic> json) {
    creationDate = json['CreationDate'] ?? '';
    deliveryDate = json['DeliveryDate'] ?? '';
    deliveryTime = json['DeliveryTime'] ?? '';
    description = json['Description'] ?? '';
    dueAmount = json['DueAmount'] ?? 0.0;
    dutDateType = json['DutDateType'] ?? 0;
    employeeId = json['EmployeeId'] ?? 0;
    fixedBudgetAmount = json['FixedBudgetAmount'] ?? 0.0;
    hourlyRate = json['HourlyRate'] ?? 0.0;
    id = json['Id'] ?? 0;
    imageServerUrl = json['ImageServerUrl'] ?? '';
    isFixedPrice = json['IsFixedPrice'] ?? false;
    isInPersonOrOnline = json['IsInPersonOrOnline'] ?? false;
    isReadMessage = json['IsReadMessage'] ?? false;
    jobCategory = json['JobCategory'] ?? '';
    latitude = json['Latitude'] ?? 0.0;
    longitude = json['Longitude'] ?? 0.0;
    netTotalAmount = json['NetTotalAmount'] ?? 0.0;
    paidAmount = json['PaidAmount'] ?? 0.0;
    ownerImageUrl = json['OwnerImageUrl'] ?? '';
    ownerName = json['OwnerName'] ?? '';
    remarks = json['Remarks'] ?? '';
    preferedLocation = json['PreferedLocation'] ?? '';
    requirements = json['Requirements'] ?? '';
    skill = json['Skill'] ?? '';
    status = json['Status'] ?? '';
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    title = json['Title'] ?? '';
    thumbnailPath = json['ThumbnailPath'] ?? '';
    totalAcceptedNumber = json['TotalAcceptedNumber'] ?? 0;
    totalBidsNumber = json['TotalBidsNumber'] ?? 0;
    totalCompletedNumber = json['TotalCompletedNumber'] ?? 0;
    totalHours = json['TotalHours'] ?? 0;
    updatedDate = json['UpdatedDate'] ?? '';
    userId = json['UserId'] ?? 0;
    workerNumber = json['WorkerNumber'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CreationDate'] = this.creationDate;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['Description'] = this.description;
    data['DueAmount'] = this.dueAmount;
    data['DutDateType'] = this.dutDateType;
    data['EmployeeId'] = this.employeeId;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['HourlyRate'] = this.hourlyRate;
    data['Id'] = this.id;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['IsReadMessage'] = this.isReadMessage;
    data['JobCategory'] = this.jobCategory;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['PaidAmount'] = this.paidAmount;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerName'] = this.ownerName;
    data['Remarks'] = this.remarks;
    data['PreferedLocation'] = this.preferedLocation;
    data['Requirements'] = this.requirements;
    data['Skill'] = this.skill;
    data['Status'] = this.status;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['Title'] = this.title;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['TotalAcceptedNumber'] = this.totalAcceptedNumber;
    data['TotalBidsNumber'] = this.totalBidsNumber;
    data['TotalCompletedNumber'] = this.totalCompletedNumber;
    data['TotalHours'] = this.totalHours;
    data['UpdatedDate'] = this.updatedDate;
    data['UserId'] = this.userId;
    data['WorkerNumber'] = this.workerNumber;
    return data;
  }
}
