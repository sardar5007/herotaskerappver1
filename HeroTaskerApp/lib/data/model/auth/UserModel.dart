class UserModel {
  String address;
  int bankVerifiedStatus;
  String briefBio;
  String communityId;
  int coverImageId;
  String coverImageUrl;
  String dateCreatedLocal;
  String dateofBirth;
  String email;
  String firstName;
  String headline;
  int id;
  bool isEmailVerified;
  bool isFirstLogin;
  bool isMobileNumberVerified;
  bool isOnline;
  bool isProfileImageVerified;
  String lastLoginDate;
  String lastName;
  String mobileNumber;
  String name;
  int nationalIDVerifiedStatus;
  int profileImageId;
  String profileImageUrl;
  String status;
  int unreadNotificationCount;
  String userName;
  String userProfileUrl;

  UserModel(
      {this.address,
      this.bankVerifiedStatus,
      this.briefBio,
      this.communityId,
      this.coverImageId,
      this.coverImageUrl,
      this.dateCreatedLocal,
      this.dateofBirth,
      this.email,
      this.firstName,
      this.headline,
      this.id,
      this.isEmailVerified,
      this.isFirstLogin,
      this.isMobileNumberVerified,
      this.isOnline,
      this.isProfileImageVerified,
      this.lastLoginDate,
      this.lastName,
      this.mobileNumber,
      this.name,
      this.nationalIDVerifiedStatus,
      this.profileImageId,
      this.profileImageUrl,
      this.status,
      this.unreadNotificationCount,
      this.userName,
      this.userProfileUrl});

  UserModel.fromJson(Map<String, dynamic> json) {
    address = json['Address'] ?? '';
    bankVerifiedStatus = json['BankVerifiedStatus'] ?? 0;
    briefBio = json['BriefBio'] ?? '';
    communityId = json['CommunityId'] ?? '0';
    coverImageId = json['CoverImageId'] ?? 0;
    coverImageUrl = json['CoverImageUrl'] ?? '';
    dateCreatedLocal = json['DateCreatedLocal'] ?? '';
    dateofBirth = json['DateofBirth'] ?? '';
    email = json['Email'] ?? '';
    firstName = json['FirstName'] ?? '';
    headline = json['Headline'] ?? '';
    id = json['Id'] ?? 0;
    isEmailVerified = json['IsEmailVerified'] ?? false;
    isFirstLogin = json['IsFirstLogin'] ?? false;
    isMobileNumberVerified = json['IsMobileNumberVerified'] ?? false;
    isOnline = json['IsOnline'] ?? false;
    isProfileImageVerified = json['IsProfileImageVerified'] ?? false;
    lastLoginDate = json['LastLoginDate'] ?? '';
    lastName = json['LastName'] ?? '';
    mobileNumber = json['MobileNumber'] ?? '';
    name = json['Name'] ?? '';
    nationalIDVerifiedStatus = json['NationalIDVerifiedStatus'] ?? 0;
    profileImageId = json['ProfileImageId'] ?? 0;
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    status = json['Status'] ?? '';
    unreadNotificationCount = json['UnreadNotificationCount'] ?? 0;
    userName = json['UserName'] ?? '';
    userProfileUrl = json['UserProfileUrl'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Address'] = this.address;
    data['BankVerifiedStatus'] = this.bankVerifiedStatus;
    data['BriefBio'] = this.briefBio;
    data['CommunityId'] = this.communityId;
    data['CoverImageId'] = this.coverImageId;
    data['CoverImageUrl'] = this.coverImageUrl;
    data['DateCreatedLocal'] = this.dateCreatedLocal;
    data['DateofBirth'] = this.dateofBirth;
    data['Email'] = this.email;
    data['FirstName'] = this.firstName;
    data['Headline'] = this.headline;
    data['Id'] = this.id;
    data['IsEmailVerified'] = this.isEmailVerified;
    data['IsFirstLogin'] = this.isFirstLogin;
    data['IsMobileNumberVerified'] = this.isMobileNumberVerified;
    data['IsOnline'] = this.isOnline;
    data['IsProfileImageVerified'] = this.isProfileImageVerified;
    data['LastLoginDate'] = this.lastLoginDate;
    data['LastName'] = this.lastName;
    data['MobileNumber'] = this.mobileNumber;
    data['Name'] = this.name;
    data['NationalIDVerifiedStatus'] = this.nationalIDVerifiedStatus;
    data['ProfileImageId'] = this.profileImageId;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['Status'] = this.status;
    data['UnreadNotificationCount'] = this.unreadNotificationCount;
    data['UserName'] = this.userName;
    data['UserProfileUrl'] = this.userProfileUrl;
    return data;
  }
}
