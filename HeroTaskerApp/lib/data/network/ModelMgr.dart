//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/DelPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/rating/PendingReviewRatingByUserIdAPIModel.dart';

class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      //  misc
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, UserDeviceAPIModel)) {
      return UserDeviceAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, GetPicAPIModel)) {
      return GetPicAPIModel.fromJson(json) as T;
    } else if (identical(T, SavePicAPIModel)) {
      return SavePicAPIModel.fromJson(json) as T;
    } else if (identical(T, DelPicAPIModel)) {
      return DelPicAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, RegProfileAPIModel)) {
      return RegProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, PendingReviewRatingByUserIdAPIModel)) {
      return PendingReviewRatingByUserIdAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostTask1APIModel)) {
      return PostTask1APIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, ParentAllCatAPIModel)) {
      return ParentAllCatAPIModel.fromJson(json) as T;
    } else if (identical(T, ChildAllCatAPIModel)) {
      return ChildAllCatAPIModel.fromJson(json) as T;
    } else if (identical(T, DelTaskAPIModel)) {
      return DelTaskAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
