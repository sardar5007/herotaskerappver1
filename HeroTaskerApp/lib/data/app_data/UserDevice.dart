import 'dart:io';

import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:flutter/services.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:device_info/device_info.dart';

class UserDevice with Mixin {
  static final UserDevice _appData = new UserDevice._internal();
  factory UserDevice() {
    return _appData;
  }
  UserDevice._internal();

  var androidInfo;
  var iosInfo;

  setUserDevice() async {
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        androidInfo = await deviceInfoPlugin.androidInfo;
      } else if (Platform.isIOS) {
        iosInfo = await deviceInfoPlugin.iosInfo;
      }
    } on PlatformException {
      log('Failed to get platform version');
    }
  }
}

final userDevice = UserDevice();
