import 'package:mypkg/controller/Mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefMgr with Mixin {
  static final PrefMgr shared = PrefMgr._internal();
  factory PrefMgr() {
    return shared;
  }

  PrefMgr._internal();

  //
  setPrefStr(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, val);
    } catch (e) {}
  }

  getPrefStr(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {}
    return null;
  }
}
