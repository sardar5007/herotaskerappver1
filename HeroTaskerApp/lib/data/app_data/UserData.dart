import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/UserModel.dart';

enum UserType { Seeker, Poster }

class UserData {
  static final UserData _appData = new UserData._internal();
  UserModel userModel;

  int communityId; //  for switching into cus/introducer dashboard

  factory UserData() {
    return _appData;
  }
  UserData._internal();

  setUserModel() async {
    userModel = await DBMgr.shared.getUserProfile();
    try {
      communityId = int.parse(userModel.communityId);
    } catch (e) {
      communityId = 1;
    }
  }

  destroyUser() {
    userModel = null;
  }
}

final userData = UserData();

extension CatExtension on UserType {
  int get type {
    switch (this) {
      case UserType.Seeker: //  WORKER
        return 1;
      case UserType.Poster: //  EMPLOYER
        return 4;
      default:
        return 0;
    }
  }

  void talk() {
    print('munir');
  }
}
