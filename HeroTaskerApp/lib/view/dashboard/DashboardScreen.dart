import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/findworks/FindWorksPage.dart';
import 'package:aitl/view/dashboard/messages/MessagesPage.dart';
import 'package:aitl/view/dashboard/more/MorePage.dart';
import 'package:aitl/view/dashboard/mytasks/MyTasksPage.dart';
import 'package:aitl/view/dashboard/post_task/PostTaskListPage.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/api/misc/pending_reviewrating_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BottomTabbarController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashboardScreen extends StatefulWidget {
  static final int TAB_POSTTASK = 0;
  static final int TAB_MYTASK = 1;
  static final int TAB_FINDWORKS = 2;
  static final int TAB_MESSAGES = 3;
  static final int TAB_MORE = 4;

  @override
  State createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with Mixin, APIStateListener, StateListener {
  final bottomBarController = new BottomTabbarController();

  final List<Widget> listTabbar = [];

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        moveTabIndex(data ?? 0);
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.FcmDeviceInfoAPIModel) {
        //log(model);
      } else if (apiType == APIType.PendingReviewRatingByUserIdAPIModel) {
        //log(model);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  moveTabIndex(int index) {
    bottomBarController.setSelectedIndex(selectValue: index);
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      await MiscViewModel().fetchDataFCMDeviceInfo(context: context);
    } catch (e) {}
    try {
      PendingReviewRatingByUserIDViewModel().fetchData(context: context);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    listTabbar.add(PostTaskListPage());
    listTabbar.add(MyTasksPage());
    listTabbar.add(FindWorksPage());
    listTabbar.add(MessagesPage());
    listTabbar.add(MorePage());

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: MyTheme.bgColor,
      body: Obx(() => listTabbar[bottomBarController.selectedItem.toInt()]),
      bottomNavigationBar: Obx(
        () => Theme(
          data: Theme.of(context).copyWith(
            canvasColor: MyTheme.bgColor,
          ), // sets the inactive color of the `BottomNavigationBar`
          child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: ImageIcon(
                      AssetImage("assets/images/tabbar/ic_new_case.png")),
                  label: "Post a task",
                ),
                BottomNavigationBarItem(
                  icon:
                      ImageIcon(AssetImage("assets/images/tabbar/ic_task.png")),
                  label: "My Tasks",
                ),
                BottomNavigationBarItem(
                  icon: ImageIcon(
                      AssetImage("assets/images/tabbar/ic_search.png")),
                  label: "Find Works",
                ),
                BottomNavigationBarItem(
                  icon: ImageIcon(
                      AssetImage("assets/images/tabbar/ic_message.png")),
                  label: "Messages",
                ),
                BottomNavigationBarItem(
                  icon:
                      ImageIcon(AssetImage("assets/images/tabbar/ic_more.png")),
                  label: "More",
                ),
              ],
              currentIndex: bottomBarController.selectedItem.value,
              //backgroundColor: Colors.blue,
              showUnselectedLabels: true,
              unselectedItemColor: Colors.black,
              selectedItemColor: MyTheme.redColor,
              iconSize: 25,
              onTap: (index) {
                bottomBarController.setSelectedIndex(selectValue: index);
              },
              elevation: MyTheme.appbarElevation),
        ),
      ),
    );
  }
}
