import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/status/StatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/MatBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/dashboard/mytasks/taskinfosearch_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'BaseMyTask.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyTasksPage extends StatefulWidget {
  @override
  State createState() => _MyTasksPageState();
}

class _MyTasksPageState extends BaseMyTaskStatefullState<MyTasksPage>
    with APIStateListener, SingleTickerProviderStateMixin {
  List<TaskModel> listTaskModel = [];

  //  search stuff start
  //  0
  //  Sometime rebuilding whole screen might not be desirable with setState((){})
  //  for this situation you can wrap searchables with ValuelistenableBuilder widget.

  ValueNotifier<List<TaskModel>> filtered = ValueNotifier<List<TaskModel>>([]);
  FocusNode searchFocus = FocusNode();
  final searchText = TextEditingController();
  bool isSearchIconClicked = false;
  bool searching = false;
  //  search stuff end

  TabController tabController;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int page = 0;
  int count = AppConfig.page_limit;

  //  tab stuff start here
  int status = StatusCfg.TASK_STATUS_ALL;
  int totalTabs = 6;

  double distance;
  double fromPrice;
  double toPrice;
  double lat;
  double lng;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.TaskInfoSearchAPIModel) {
        //log(model);
        if (model != null && mounted) {
          final List<dynamic> locations = model.responseData.locations;
          if (locations != null) {
            //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
            if (locations.length != count) {
              isPageDone = true;
            }
            try {
              for (TaskModel task in locations) {
                listTaskModel.add(task);
              }
              setState(() {});
            } catch (e) {
              log(e.toString());
            }
          } else {}
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  onLazyLoadAPI() async {
    setState(() {
      isLoading = true;
    });
    await TaskInfoSearchViewModel().fetchData(
      context: context,
      page: page,
      count: count,
      searchText: searchText.text.trim(),
      distance: distance,
      fromPrice: fromPrice,
      toPrice: toPrice,
      lat: lat,
      lng: lng,
      status: status,
    );
    setState(() {
      isLoading = false;
    });
  }

  Future<void> refreshData() async {
    setState(() {
      isSearchIconClicked = false;
      searchText.clear();
      searching = false;
      filtered.value = [];
      if (searchFocus.hasFocus) searchFocus.unfocus();
      //
      page = 0;
      isPageDone = false;
      isLoading = true;
      listTaskModel.clear();
    });
    onLazyLoadAPI();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      listTaskModel = null;
      rxDestroyTask();
    } catch (e) {}
    try {
      tabController.dispose();
      tabController = null;
      searchText.dispose();
    } catch (e) {}
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      tabController = new TabController(vsync: this, length: totalTabs);
      tabController.addListener(() {
        if (!isLoading) {
          log('top tab index = ' + tabController.index.toString());
          onTopTabbarIndexChanged(tabController.index, (map) {
            status = map['status'];
            refreshData();
          });
        }
      });
    } catch (e) {}
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: totalTabs,
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            //automaticallyImplyLeading: !isSearchIconClicked,
            leadingWidth: (!isSearchIconClicked) ? 0 : 56,
            leading: (!isSearchIconClicked)
                ? SizedBox()
                : IconButton(
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      isSearchIconClicked = !isSearchIconClicked;
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                    },
                    icon: Icon(Icons.arrow_back)),
            title: (!isSearchIconClicked)
                ? Txt(
                    txt: 'My tasks',
                    txtColor: Colors.black,
                    txtSize: MyTheme.appbarTitleFontSize,
                    txtAlign: TextAlign.center,
                    isBold: true)
                : drawSearchbar(searchText, (text) {
                    if (text.length > 0) {
                      searching = true;
                      filtered.value = [];
                      listTaskModel.forEach((locModel) {
                        if (locModel.title
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase()) ||
                            locModel.ownerName
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase())) {
                          filtered.value.add(locModel);
                        }
                      });
                    } else {
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                    }
                  }),
            centerTitle: false,
            actions: <Widget>[
              (!isSearchIconClicked)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            color: Colors.black,
                            size: 30,
                          ),
                          onPressed: () {
                            isSearchIconClicked = !isSearchIconClicked;
                            setState(() {});
                          }),
                    )
                  : SizedBox()
            ],
            bottom: drawAppbarNavBar(tabController, (topTabindex) {
              if (!isLoading) {
                tabController.index = topTabindex;
              }
            }),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              physics: (isLoading)
                  ? NeverScrollableScrollPhysics()
                  : AlwaysScrollableScrollPhysics(),
              controller: tabController,
              children: <Widget>[
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listTaskModel.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    page++;
                    onLazyLoadAPI();
                  }
                }
                return true;
              },
              child: ValueListenableBuilder<List>(
                valueListenable: filtered,
                builder: (context, value, _) {
                  return Theme(
                    data: MyTheme.refreshIndicatorTheme,
                    child: RefreshIndicator(
                      onRefresh: refreshData,
                      child: ListView.builder(
                        addAutomaticKeepAlives: true,
                        cacheExtent: AppConfig.page_limit.toDouble(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        //primary: false,
                        itemCount: searching
                            ? filtered.value.length
                            : listTaskModel.length,
                        itemBuilder: (BuildContext context, int index) {
                          return drawCardInfo(searching
                              ? filtered.value[index]
                              : listTaskModel[index]);
                        },
                      ),
                    ),
                  );
                },
              ),
            )
          : (!isLoading)
              ? Center(
                  child: ListView(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    shrinkWrap: true,
                    children: [
                      Container(
                        //width: getWP(context, 100),
                        //height: getHP(context, 48),
                        child: SvgPicture.asset(
                          'assets/images/nf/nf_tasks.svg',
                          //fit: BoxFit.fill,
                        ),
                      ),
                      //SizedBox(height: 40),
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 10, left: 50, right: 50),
                        child: Container(
                          child: Txt(
                            txt: "Looks like you haven't created any task yet",
                            txtColor: MyTheme.mycasesNFBtnColor,
                            txtSize: MyTheme.txtSize + .5,
                            txtAlign: TextAlign.center,
                            isBold: false,
                            //txtLineSpace: 1.5,
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        width: getWP(context, 20),
                        child: drawMatBtn("Create Now!", () {}),
                      ),
                    ],
                  ),
                )
              : Container(),
    );
  }
}
