import 'package:aitl/config/status/StatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/dashboard/mytasks/TaskDetailsScreen.dart';
import 'package:aitl/view/dashboard/post_task/add/AddTask1Screen.dart';
import 'package:aitl/view/dashboard/post_task/add/BaseAddTask.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/classes/Common.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

abstract class BaseMyTaskStatefullState<T extends StatefulWidget>
    extends State<T> with Mixin {
  final TaskController taskController = Get.put(TaskController());

  bool isLoading = false;

//  getX rx cache model
  rxCacheTaskModel(model) async {
    try {
      taskController.setTaskModel(model);
    } catch (e) {
      log(e.toString());
    }
  }

  rxDestroyTask() async {
    try {
      taskController.setTaskModel(null);
      taskController.taskImages = null;
    } catch (e) {
      log(e.toString());
    }
  }

  drawSearchbar(TextEditingController searchText, Function(String) onChange) {
    return TextField(
      controller: searchText,
      autofocus: true,
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.search,
      onChanged: (value) => onChange(value),
      autocorrect: false,
      style: TextStyle(
        color: Colors.black,
        fontSize: 20,
      ),
      decoration: InputDecoration(
        prefixIcon: Icon(
          Icons.search,
          color: Colors.black,
        ),
        suffixIcon: IconButton(
            onPressed: () => onChange(""),
            icon: Icon(
              Icons.close,
              color: Colors.black,
            )),
        hintText: "Search by title",
        hintStyle: new TextStyle(
          color: Colors.grey,
          fontSize: 20,
          //height: MyTheme.txtLineSpace,
        ),
        border: InputBorder.none,
      ),
    );
  }

  drawAppbarNavBar(TabController tabController, Function callback) {
    return PreferredSize(
      preferredSize: Size.fromHeight(getHP(context, 12)),
      child: Container(
        color: MyTheme.brandColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.grey,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 15, bottom: 15),
                child: Center(
                  child: Txt(
                      txt: "View the ongoing Task",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
              ),
            ),
            TabBar(
              onTap: (index2) {
                if (isLoading) {
                  callback(index2);
                }
              },
              controller: tabController,
              isScrollable: true,
              indicatorColor: Colors.black,
              unselectedLabelColor: Colors.white54,
              labelColor: Colors.white,
              /*indicator: UnderlineTabIndicator(
                                  borderSide:
                                      BorderSide(width: 5.0, color: Colors.white),
                                  insets: EdgeInsets.symmetric(horizontal: 16.0)),*/
              tabs: [
                Tab(
                    child: Txt(
                        txt: "All",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Tab(
                    child: Txt(
                        txt: "Posted",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Tab(
                    child: Txt(
                        txt: "Draft",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Tab(
                    child: Txt(
                        txt: "Assigned",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Tab(
                    child: Txt(
                        txt: "Offered",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Tab(
                    child: Txt(
                        txt: "Completed",
                        txtColor: null,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
              ],
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(height: 4)
          ],
        ),
      ),
    );
  }

  onTopTabbarIndexChanged(int index, Function(Map<String, dynamic>) callback) {
    switch (index) {
      case 0:
        callback({'status': StatusCfg.TASK_STATUS_ALL});
        break;
      case 1:
        callback({'status': StatusCfg.TASK_STATUS_ACTIVE});
        break;
      case 2:
        callback({'status': StatusCfg.TASK_STATUS_DRAFT});
        break;
      case 3:
        callback({'status': StatusCfg.TASK_STATUS_ACCEPTED});
        break;
      case 4:
        callback({'status': StatusCfg.TASK_STATUS_PENDINGTASK});
        break;
      case 5:
        callback({'status': StatusCfg.TASK_STATUS_PAYMENTED});
        break;
      default:
    }
  }

  postTaskScreen(TaskModel taskModel) async {
    try {
      await rxCacheTaskModel(taskModel);
    } catch (e) {}

    Get.to(
      () => AddTask1Screen(index: 0),
    ).then(
      (value) {
        if (value != null) {
          switch (value) {
            case TaskCancelType.onBack:
              final StateProvider _stateProvider = new StateProvider();
              _stateProvider.notify(
                  ObserverState.STATE_RELOAD_TAB, DashboardScreen.TAB_MYTASK);
              break;
            case TaskCancelType.onDelTask:
              final StateProvider _stateProvider = new StateProvider();
              _stateProvider.notify(
                  ObserverState.STATE_RELOAD_TAB, DashboardScreen.TAB_MYTASK);
              break;
            case TaskCancelType.onTaskDone:
              final StateProvider _stateProvider = new StateProvider();
              _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                  DashboardScreen.TAB_FINDWORKS);
              break;
            default:
              break;
          }
        }
      },
    );
  }

  taskDetailScreen(TaskModel taskModel) {
    Get.to(() => TaskDetailsScreen());
  }

  drawCardInfo(TaskModel taskModel) {
    final priceTxt = (taskModel.isFixedPrice)
        ? taskModel.fixedBudgetAmount.toString()
        : taskModel.hourlyRate.toString() + '/hr';
    //
    var status = taskModel.status;
    try {
      if (int.parse(status) > 0) {
        status = StatusCfg().getSatus(int.parse(status));
      }
    } catch (e) {
      status = taskModel.status;
    }
    //
    String preferedLocation = taskModel.preferedLocation;
    if (taskModel.isInPersonOrOnline) preferedLocation = "Remote";
    //
    Color ribbonColor = MyTheme.airBlueColor;
    if (status.toString().toLowerCase() == "active") {
      ribbonColor = MyTheme.airGreenColor;
    }
    //
    Color statusColor = MyTheme.dGreenColor;
    String statusAddtionalInfo = "";
    if (status.toString().toLowerCase() == "active") {
      if (taskModel.totalBidsNumber > 0) {
        statusAddtionalInfo = statusAddtionalInfo +
            "- " +
            taskModel.totalBidsNumber.toString() +
            " offer";
      } else {
        statusAddtionalInfo = "";
      }
    }
    //

    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: GestureDetector(
        onTap: () {
          if (taskModel.status.toString().toLowerCase() == "draft") {
            postTaskScreen(taskModel);
          } else {
            taskDetailScreen(taskModel);
          }
        },
        child: Container(
          color: Colors.transparent,
          child: IntrinsicHeight(
            child: Row(
              children: [
                Expanded(
                    child: Container(
                  width: getWP(context, 1.5),
                  color: ribbonColor,
                )),
                Container(
                  width: getWP(context, 98.5),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, bottom: 10, left: 20, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(
                              flex: 2,
                              child: Txt(
                                  txt: taskModel.title,
                                  txtColor: MyTheme.gray4Color,
                                  txtSize: MyTheme.txtSize + .3,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                            Expanded(
                              child: Container(
                                //color: Colors.black,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Flexible(
                                      child: getCurIcon(),
                                    ),
                                    Flexible(
                                      child: Txt(
                                        txt: priceTxt,
                                        txtColor: MyTheme.gray5Color,
                                        txtSize: MyTheme.txtSize + .4,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                        isOverflow: true,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Icon(Icons.location_on_outlined),
                                        SizedBox(width: 5),
                                        Flexible(
                                          child: Txt(
                                            txt: preferedLocation,
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .2,
                                            txtAlign: TextAlign.start,
                                            isBold: false,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10),
                                    Row(
                                      children: [
                                        Icon(Icons.calendar_today_outlined),
                                        SizedBox(width: 5),
                                        Flexible(
                                          child: Txt(
                                            txt: Common.getFormatedDate(
                                                mDate: DateFormat("dd-MMM-yyyy")
                                                    .parse(
                                                        taskModel.deliveryDate),
                                                format: "EEE MMM dd, yyyy"),
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .2,
                                            txtAlign: TextAlign.start,
                                            isBold: false,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Txt(
                                              txt: status,
                                              txtColor: statusColor,
                                              txtSize: MyTheme.txtSize - .3,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        ),
                                        SizedBox(width: 2),
                                        Flexible(
                                          child: Txt(
                                              txt: statusAddtionalInfo,
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .3,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Flexible(
                                child: Container(
                                  width: getWP(context, 10),
                                  height: getWP(context, 10),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image:
                                          NetworkImage(taskModel.ownerImageUrl),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.rectangle,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
