import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';

class TaskDetailsScreen extends StatelessWidget {
  const TaskDetailsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          /*leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
              Get.back();
            
      
        },
      ),*/
          title: Txt(
              txt: 'Post Details',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container();
  }
}
