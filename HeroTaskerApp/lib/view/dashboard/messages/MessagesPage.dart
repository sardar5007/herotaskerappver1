import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/status/StatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/dashboard/mytasks/taskinfosearch_view_model.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

class MessagesPage extends StatefulWidget {
  @override
  State createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with Mixin, APIStateListener {
  final searchText = TextEditingController();

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int page = 0;
  int count = AppConfig.page_limit;

  //  tab stuff start here
  int status = StatusCfg.STATUS_ALL_PRIVATEMESSAGE;
  int totalTabs = 5;

  double distance;
  double fromPrice;
  double toPrice;
  double lat;
  double lng;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.UserDeviceAPIModel) {
        //log(model);
        if (model != null && mounted) {
          //
        }
      } else if (apiType == APIType.TaskInfoSearchAPIModel) {
        //log(model);
        if (model != null && mounted) {
          //
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  onLazyLoadAPI() async {
    setState(() {
      isLoading = true;
    });
    await TaskInfoSearchViewModel().fetchData(
      context: context,
      page: page,
      count: count,
      searchText: searchText.text.trim(),
      distance: distance,
      fromPrice: fromPrice,
      toPrice: toPrice,
      lat: lat,
      lng: lng,
      status: status,
    );
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      MiscViewModel().fetchDataUserDevices(context: context);
    } catch (e) {}

    onLazyLoadAPI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'Find works',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {}
}
