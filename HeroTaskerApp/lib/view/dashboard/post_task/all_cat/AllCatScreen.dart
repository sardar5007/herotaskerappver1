import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildUserTaskCategorysModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentUserTaskCategorysModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/AddTask1Screen.dart';
import 'package:aitl/view/widgets/ExpansionTileCard.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/dashboard/posttask/allcat_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
//import 'package:expansion_tile_card/expansion_tile_card.dart';
import '../../DashboardScreen.dart';
import 'BaseAllCat.dart';
import 'package:get/get.dart';

class AllCatScreen extends StatefulWidget {
  final int index;
  const AllCatScreen({Key key, @required this.index}) : super(key: key);
  @override
  State createState() => _AllCatState();
}

class _AllCatState extends BaseAllCatStatefullState<AllCatScreen>
    with APIStateListener {
  List<ParentUserTaskCategorysModel> listParentUserTaskCategorysModel;
  List<ChildUserTaskCategorysModel> listChildUserTaskCategorysModel;
  APIStateProvider _apiStateProvider;

  int parentId = 1;
  int indeXCardKey = 0;
  String appbarTitle = "All Categories";

  List<GlobalKey<ExpansionTileCardState>> listXCardKey = [];
  final ScrollController _scrollController = new ScrollController();

  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.ParentAllCatAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              listParentUserTaskCategorysModel = (model as ParentAllCatAPIModel)
                  .responseData
                  .userTaskCategorys;
              appbarTitle = listParentUserTaskCategorysModel[0].name;
              wsChildCat(1);
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.ChildAllCatAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              listChildUserTaskCategorysModel =
                  (model as ChildAllCatAPIModel).responseData.userTaskCategorys;
              setState(() {});
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      listXCardKey = null;
      listParentUserTaskCategorysModel = null;
      listChildUserTaskCategorysModel = null;
    } catch (e) {}
    try {
      _scrollController.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      AllCatViewModel().fetchDataParentCat(context: context, parentId: 0);
    } catch (e) {}
  }

  wsChildCat(int parentId) {
    try {
      AllCatViewModel().fetchDataChildCat(context: context, parentId: parentId);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: appbarTitle,
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    indeXCardKey = 0;
    listXCardKey = [];

    Future.delayed(Duration.zero, () {
      try {
        if (_scrollController.hasClients) {
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        }
      } catch (e) {}
    });

    return (listChildUserTaskCategorysModel != null)
        ? Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: getWP(context, 40),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        for (var item in listParentUserTaskCategorysModel)
                          GestureDetector(
                            onTap: () {
                              parentId = item.id;
                              appbarTitle = item.name;
                              wsChildCat(item.id);
                            },
                            child: Container(
                              height: getHP(context, 20),
                              child: Card(
                                elevation: 1,
                                color: parentId == item.id
                                    ? MyTheme.greyColor
                                    : Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Center(
                                    child: Txt(
                                        txt: item.name,
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize + .1,
                                        txtAlign: TextAlign.center,
                                        isBold: true),
                                  ),
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    width: getWP(context, 60),
                    child: ListView(
                      controller: _scrollController,
                      shrinkWrap: true,
                      children: [
                        for (var item in listChildUserTaskCategorysModel)
                          drawDropDownBox(item),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        : SizedBox();
  }

  drawDropDownBox(ChildUserTaskCategorysModel childModel) {
    try {
      listXCardKey.add(GlobalKey());
      final GlobalKey<ExpansionTileCardState> cardKey =
          listXCardKey[indeXCardKey];
      indeXCardKey++;

      return Padding(
        padding: const EdgeInsets.all(2),
        child: ExpansionTileCard(
          //  https://pub.dev/packages/expansion_tile_card/example
          initiallyExpanded: true,
          elevation: 0,
          baseColor: MyTheme.brandColor,
          expandedColor: MyTheme.brandColor,
          key: cardKey,
          //leading: CircleAvatar(child: Text('A')),
          title: Txt(
              txt: childModel.name,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          //subtitle: Text('I expand!'),
          children: <Widget>[
            Container(
              color: Colors.white,
              child: drawExpandedChildGrid(
                  childModel.userTaskItemResponseModelList),
            ),
          ],
        ),
      );
    } catch (e) {
      return SizedBox();
    }
  }

  drawExpandedChildGrid(List<UserTaskItemResponseModelList> childList) {
    var _crossAxisSpacing = 10;
    var _screenWidth = getWP(context, 60);
    var _crossAxisCount = 2;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = getHP(context, 10);
    var _aspectRatio = _width / cellHeight;

    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: childList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: _aspectRatio),
      itemBuilder: (context, index) {
        UserTaskItemResponseModelList model = childList[index];

        return GestureDetector(
          onTap: () async {
            Get.to(
              () => AddTask1Screen(index: index),
            ).then((value) {
              if (value != null) {
                if (value) {
                  final StateProvider _stateProvider = new StateProvider();
                  _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                      DashboardScreen.TAB_FINDWORKS);
                }
              }
            });
          },
          child: Card(
            elevation: .5,
            color: MyTheme.greyColor,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Txt(
                    txt: model.name,
                    txtColor: MyTheme.timelineTitleColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    maxLines: 2,
                    txtLineSpace: 1.2,
                    isBold: false),
              ),
            ),
          ),
        );
      },
    );
  }
}
