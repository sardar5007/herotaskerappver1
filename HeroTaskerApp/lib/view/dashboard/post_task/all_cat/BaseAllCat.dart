import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

abstract class BaseAllCatStatefullState<T extends StatefulWidget>
    extends State<T> with Mixin {
  BaseAllCatStatefullState() {
    // Parent constructor
  }

  drawLeftListView() {}

  drawRightListView() {}
}
