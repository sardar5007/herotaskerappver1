import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/dashboard/post_task/add/AddTask1Screen.dart';
import 'package:aitl/view/dashboard/post_task/all_cat/AllCatScreen.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/dashboard/poster/PostTaskHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'add/BaseAddTask.dart';

class PostTaskListPage extends StatefulWidget {
  @override
  State createState() => _PostTaskListPageState();
}

class _PostTaskListPageState extends State<PostTaskListPage> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'Post a task',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
          bottom: PreferredSize(
            child: Container(
              color: MyTheme.titlePostTaskColor,
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 15, bottom: 15),
                child: Txt(
                    txt: "Select the type of job you need done from below",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
            ),
            preferredSize: Size.fromHeight(80),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawTaskList(),
            drawHireWorkButtons(),
          ],
        ),
      ),
    );
  }

  drawTaskList() {
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: PostTaskHelper.listTask.length,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (context, index) {
        final map = PostTaskHelper.listTask[index];
        final icon = map["icon"];
        final title = map["title"];
        final isCat = map['isCat'];
        return GestureDetector(
          onTap: () async {
            if (!isCat) {
              Get.to(
                () => AddTask1Screen(index: index),
              ).then((value) {
                if (value != null) {
                  switch (value) {
                    case TaskCancelType.onBack:
                      final StateProvider _stateProvider = new StateProvider();
                      _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                          DashboardScreen.TAB_MYTASK);
                      break;
                    case TaskCancelType.onDelTask:
                      final StateProvider _stateProvider = new StateProvider();
                      _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                          DashboardScreen.TAB_MYTASK);
                      break;
                    case TaskCancelType.onTaskDone:
                      final StateProvider _stateProvider = new StateProvider();
                      _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                          DashboardScreen.TAB_FINDWORKS);
                      break;
                    default:
                      break;
                  }
                }
              });
            } else {
              Get.to(
                () => AllCatScreen(index: index),
              ).then((value) {
                if (value != null) {
                  if (value) {
                    final StateProvider _stateProvider = new StateProvider();
                    _stateProvider.notify(ObserverState.STATE_RELOAD_TAB,
                        DashboardScreen.TAB_FINDWORKS);
                  }
                }
              });
            }
          },
          child: Card(
            elevation: 0,
            margin: new EdgeInsets.all(10),
            //color: Colors.black,
            child: new Column(
              //mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  child: Container(
                    //width: getWP(context, 25),
                    //height: getWP(context, 25),
                    //color: Colors.black,
                    child: SvgPicture.asset(
                      icon,
                      //width: 100,
                      //height: 100,
                      //fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Txt(
                    txt: title,
                    txtColor: MyTheme.iconTextColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    maxLines: 2,
                    //txtLineSpace: 1.2,
                    isBold: false),
                //just for testing, will fill with image later
              ],
            ),
          ),
        );
      },
    );
  }

  drawHireWorkButtons() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Flexible(
              child: MMBtn(
                txt: "I want to work",
                //txtColor: Colors.white,
                //bgColor: accentColor,
                width: getWP(context, 40),
                height: getHP(context, 6),
                radius: 20,
                callback: () {},
              ),
            ),
            //SizedBox(width: 10),
            Flexible(
              child: MMBtn(
                txt: "I want to hire",
                //txtColor: Colors.white,
                //bgColor: accentColor,
                width: getWP(context, 40),
                height: getHP(context, 6),
                radius: 20,
                callback: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
