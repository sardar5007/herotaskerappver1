import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/BaseAddTask.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/dashboard/posttask/taskpic_view_model.dart';
import 'package:aitl/view_model/api/misc/emailnoti_view_model.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTask4Screen extends StatefulWidget {
  final String dueDate;
  const AddTask4Screen({
    Key key,
    @required this.dueDate,
  }) : super(key: key);
  @override
  State createState() => _AddTask4ScreenState();
}

const minEstBudget = 50;
const maxEstBudget = 1000000;

class _AddTask4ScreenState extends BaseAddTaskStatefullState<AddTask4Screen>
    with APIStateListener {
  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.UserDeviceAPIModel) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiType == APIType.EmailNoti) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiType == APIType.SavePicAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      rxDestroyTask();
    } catch (e) {}

    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      //  to save pic api
      for (var map in taskController.taskImages.value) {
        if (taskController.getTaskModel().id != null) {
          await TaskPicViewModel().fetchDataSavePic(
            context: context,
            mediaId: map['id'],
            taskId: taskController.getTaskModel().id,
          );
        }
      }

      //  normal apis at the end calling
      await MiscViewModel().fetchDataUserDevices(
        context: context,
        eventType: 'Task Post - Submit',
      );
      //  email notification api call
      EmailNotiViewModel().fetchDataSendEmailNoti(
        context: context,
        taskId: taskController.getTaskModel().id,
      );
    } catch (e) {}
  }

  onNextClicked() {}
  onDelTaskClicked() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'Offer Accepted',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: MMArrowBtn(
              txt: "Done",
              icon: null,
              height: getHP(context, 7),
              width: getW(context),
              callback: () {
                Get.back(result: TaskCancelType.onTaskDone);
              },
            ),
          ),
          elevation: 0,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Txt(
                txt: "Congratulations " + userData.userModel.lastName + ",",
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize + 1.3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 20),
              Txt(
                txt:
                    "Your task has been posted. You 'll be notified as soon as you receive an offer.",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              )
            ],
          ),
        ),
      ),
    );
  }
}
