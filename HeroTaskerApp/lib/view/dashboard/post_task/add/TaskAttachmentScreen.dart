import 'dart:io';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/CamPicker.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/api/dashboard/posttask/taskpic_view_model.dart';
import 'package:aitl/view_model/api/misc/media_upload_file_view_model.dart';
import 'package:aitl/view_model/helper/auth/profile/ProfileHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'BaseAddTask.dart';

class TaskAttachmentScreen extends StatefulWidget {
  @override
  State createState() => _TaskAttachmentScreenState();
}

class _TaskAttachmentScreenState
    extends BaseAddTaskStatefullState<TaskAttachmentScreen>
    with APIStateListener {
  final int totalUploadLimit = 5;

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(APIObserverState state, APIType apiType, model) async {
    try {
      if (apiType == APIType.MediaUploadFilesAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            final MediaUploadFilesModel mediaModel =
                (model as MediaUploadFilesAPIModel).responseData.images[0];

            taskController.taskImages.value.add({
              'isGetPicModel': false,
              'id': mediaModel.id,
              'url': mediaModel.url
            });
            setState(() {});
          }
        }
      } else if (apiType == APIType.GetPicAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            final List<GetPicModel> listPicModel =
                (model as GetPicAPIModel).responseData.taskPictures;
            for (GetPicModel picModel in listPicModel) {
              taskController.taskImages.value.add({
                'isGetPicModel': true,
                'id': picModel.id,
                'url': picModel.thumbnailPath,
              });
            }
            setState(() {});
          }
        }
      } else if (apiType == APIType.DelPicAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            taskController.taskImages.value.removeAt(model.index);
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (taskController.getTaskModel().id != null) {
        TaskPicViewModel().fetchDataGetPics(
          context: context,
          taskId: taskController.getTaskModel().id,
        );
      }
    } catch (e) {}
  }

  onNextClicked() {}
  onDelTaskClicked() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Task attachments',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    log(taskController.taskImages.value);

    int len = 1;
    try {
      len = (taskController.taskImages.value.length < totalUploadLimit)
          ? taskController.taskImages.value.length + 1
          : taskController.taskImages.value.length;
    } catch (e) {}
    return Container(
      child: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        padding: EdgeInsets.all(10.0),
        children: List.generate(
          len,
          (index) {
            String url = "";
            int id = 0;
            var model;
            if (index < taskController.taskImages.value.length) {
              model = taskController.taskImages.value[index];
              id = model['id'];
              url = model['url'];
            } else
              model = null;

            return Card(
              child: (model != null)
                  ? Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(url),
                              fit: BoxFit.cover,
                            ),
                            shape: BoxShape.rectangle,
                          ),
                        ),
                        Positioned(
                          right: -5,
                          top: -5,
                          child: Container(
                            width: 30,
                            height: 30,
                            child: MaterialButton(
                              onPressed: () {
                                Get.dialog(
                                  ConfirmDialog(
                                      callback: () {
                                        TaskPicViewModel().fetchDataDelPic(
                                          context: context,
                                          index: index,
                                          mediaId: id,
                                        );
                                      },
                                      title: "Confirmation",
                                      msg:
                                          "Are you sure, you want to remove this attachment?"),
                                );
                              },
                              color: MyTheme.gray1Color,
                              child: Text("X",
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.black)),
                              padding: EdgeInsets.all(0),
                              shape: CircleBorder(),
                            ),
                          ),
                        ),
                      ],
                    )
                  : GestureDetector(
                      onTap: () {
                        CamPicker().showCamModal(
                          context: context,
                          prefkey: ProfileHelper.ProfilePicFG_Key,
                          isRear: false,
                          callback: (File path) {
                            if (path != null) {
                              MediaUploadFileViewModel().fetchDataPicture(
                                context: context,
                                file: path,
                              );
                            }
                          },
                        );
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyTheme.gray1Color,
                        ),
                        child: Icon(
                          Icons.add,
                          size: 100,
                          color: MyTheme.gray3Color,
                        ),
                      ),
                    ),
            );
          },
        ),
      ),
    );
  }
}
