import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/status/StatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/AddTask2Screen.dart';
import 'package:aitl/view/dashboard/post_task/add/BaseAddTask.dart';
import 'package:aitl/view/widgets/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/SwitchTitle.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view_model/api/dashboard/posttask/posttask_view_model.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/helper/dashboard/poster/PostTaskHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';

class AddTask1Screen extends StatefulWidget {
  final int index;
  const AddTask1Screen({Key key, this.index}) : super(key: key);
  @override
  State createState() => _AddTask1ScreenState();
}

class _AddTask1ScreenState extends BaseAddTaskStatefullState<AddTask1Screen>
    with APIStateListener {
  final TaskController taskController = Get.put(TaskController());

  final taskHeadline = TextEditingController();
  final taskDesc = TextEditingController();
  final taskLoc = TextEditingController();

  bool isOnlineSwitch = false;
  String taskAddress = "Search";
  Location taskCord;
  final int minLenTaskHeadline = 10;
  final int minLenTaskDesc = 25;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.DelTaskAPIModel) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              rxDestroyTask();
              Get.back(result: TaskCancelType.onDelTask);
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.PostTask1APIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              await rxCacheTaskModel(
                model.responseData.task,
              );
              Get.to(() => AddTask2Screen()).then((value) {
                if (value != null) {
                  rxDestroyTask();
                  Get.back(result: value);
                }
              });
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.UserDeviceAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      taskHeadline.dispose();
      taskDesc.dispose();
      taskLoc.dispose();
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      //taskData.listTaskImages = [];
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    if (taskController.getTaskModel().id != null) {
      isOnlineSwitch = taskController.getTaskModel().isInPersonOrOnline;
      taskHeadline.text = taskController.getTaskModel().title ?? '';
      taskDesc.text = taskController.getTaskModel().description ?? '';
      taskLoc.text = taskController.getTaskModel().preferedLocation ?? '';
    }

    try {
      if (widget.index != null) {
        final map = PostTaskHelper.listTask[widget.index];
        await MiscViewModel().fetchDataUserDevices(
          context: context,
          eventType: map['name'],
        );
      }
      MiscViewModel().fetchDataUserDevices(
        context: context,
        eventType: 'Task Post - Task Details',
      );
    } catch (e) {}
  }

  validate() {
    if (taskHeadline.text.length < 11) {
      showToast(
          msg: 'PLease enter a valid title (atleast ' +
              minLenTaskHeadline.toString() +
              ' characters)');
      return false;
    } else if (taskDesc.text.length < 26) {
      showToast(
          msg: 'PLease enter a descriptions (atleast ' +
              minLenTaskDesc.toString() +
              ' characters)');
      return false;
    } else if (!isOnlineSwitch && taskCord == null) {
      showToast(msg: 'PLease search your address');
      return false;
    }

    return true;
  }

  onNextClicked() {
    if (validate()) {
      log(taskController.getTaskModel());

      if (taskController.getTaskModel().id == null) {
        PostTaskViewModel().fetchDataPostTask1(
          context: context,
          status: StatusCfg.TASK_STATUS_DRAFT,
          taskHeadline: taskHeadline.text.trim(),
          taskDesc: taskDesc.text.trim(),
          taskLoc: taskLoc.text.trim(),
          taskCord:
              (taskCord != null) ? Location(taskCord.lat, taskCord.lng) : null,
          taskAddr: taskAddress,
          isOnlineJob: isOnlineSwitch,
        );
      } else {
        PostTaskViewModel().fetchDataPutTask1(
          context: context,
          status: StatusCfg.TASK_STATUS_DRAFT,
          taskId: taskController.getTaskModel().id,
          taskHeadline: taskHeadline.text.trim(),
          taskDesc: taskDesc.text.trim(),
          taskLoc: taskLoc.text.trim(),
          taskCord:
              (taskCord != null) ? Location(taskCord.lat, taskCord.lng) : null,
          taskAddr: taskAddress,
          isOnlineJob: isOnlineSwitch,
        );
      }
    }
  }

  onDelTaskClicked() {
    try {
      PostTaskViewModel().fetchDataDeleteTask(context: context);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          title: 'Post Task',
          icon1: '01b',
          icon2: '02',
          icon3: '03',
          isBold1: true,
          isBold2: false,
          isBold3: false,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: MMArrowBtn(
              txt: "Continue",
              icon: Icons.arrow_forward,
              height: getHP(context, 7),
              width: getW(context),
              callback: () {
                onNextClicked();
              },
            ),
          ),
          elevation: 0,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              children: [
                InputTitleBoxHT(
                  title: "Task Headline",
                  ph: "eg. Clean my 2 bedroom apartment",
                  input: taskHeadline,
                  kbType: TextInputType.text,
                  len: 50,
                  minLen: minLenTaskHeadline,
                ),
                SizedBox(height: 20),
                InputTitleBoxHT(
                  title: "Description",
                  ph: "",
                  input: taskDesc,
                  kbType: TextInputType.multiline,
                  len: 255,
                  minLen: minLenTaskDesc,
                  minLine: 1,
                  maxLine: 3,
                ),
                SizedBox(height: 20),
                SwitchTitle(
                  txt: "Is it online based job?",
                  callback: (_isSwitch) {
                    isOnlineSwitch = _isSwitch;
                    setState(() {});
                  },
                ),
                (!isOnlineSwitch)
                    ? Column(
                        children: [
                          SizedBox(height: 20),
                          InputTitleBoxHT(
                            title: "Task Location (Neighbourhood)",
                            ph: "",
                            input: taskLoc,
                            kbType: TextInputType.text,
                            len: 255,
                            minLen: 0,
                          ),
                          SizedBox(height: 5),
                          GPlacesView(
                            title: null,
                            address: taskAddress,
                            bgColor: Colors.white,
                            callback: (String _address, Location _loc) {
                              //callback(address);
                              taskAddress = _address;
                              taskCord = _loc;
                              setState(() {});
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
