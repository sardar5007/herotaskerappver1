import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/post_task/add/TaskAttachmentScreen.dart';
import 'package:aitl/view/widgets/AppbarText..dart';
import 'package:aitl/view/widgets/StepBar.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';

enum TaskCancelType {
  onBack,
  onDelTask,
  onTaskDone,
}

abstract class BaseAddTaskStatefullState<T extends StatefulWidget>
    extends State<T> with Mixin {
  final TaskController taskController = Get.put(TaskController());

  BaseAddTaskStatefullState() {
    // Parent constructor
  }

  //  abstract methods
  onNextClicked();
  onDelTaskClicked();

  //  getX rx cache model
  rxCacheTaskModel(model) async {
    try {
      taskController.setTaskModel(model);
    } catch (e) {
      log(e.toString());
    }
  }

  rxDestroyTask() async {
    try {
      taskController.setTaskModel(null);
      taskController.taskImages = null;
    } catch (e) {
      log(e.toString());
    }
  }

  drawAppbar({
    @required title,
    @required icon1,
    @required icon2,
    @required icon3,
    @required isBold1,
    @required isBold2,
    @required isBold3,
  }) {
    final totalDots = getW(context) / 15;
    return AppBar(
      elevation: MyTheme.appbarElevation,
      backgroundColor: MyTheme.bgColor,
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          try {
            if (taskController.getTaskModel().id != null) {
              Get.back(result: TaskCancelType.onBack);
            } else {
              Get.back();
            }
          } catch (e) {
            Get.back();
          }
        },
      ),
      title: Txt(
          txt: 'Post Task',
          txtColor: MyTheme.appbarTxtColor,
          txtSize: MyTheme.appbarTitleFontSize,
          txtAlign: TextAlign.center,
          isBold: true),
      centerTitle: false,
      actions: [
        IconButton(
          icon: Icon(Icons.attach_file),
          onPressed: () {
            Get.to(() => TaskAttachmentScreen());
          },
        ),
        SizedBox(width: 20),
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            if (taskController.getTaskModel().id != null) {
              Get.dialog(
                ConfirmDialog(
                    callback: () {
                      //onDelTaskClicked();
                    },
                    title: "Registration Failed!",
                    msg: "Are you sure, you want to delete this task?"),
              );
            } else {
              Get.back(result: true);
            }
          },
        ),
        SizedBox(width: 20),
        drawAppbarText(
          txt: "NEXT",
          callback: () {
            onNextClicked();
          },
        ),
        SizedBox(width: 20),
      ],
      iconTheme: IconThemeData(color: Colors.black //change your color here
          ),
      bottom: PreferredSize(
          child: Container(
            color: Colors.white,
            height: getHP(context, 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                drawStepBar(
                  icon: icon1,
                  txt: "DETAILS",
                  isBold: isBold1,
                  dotsLen: 0,
                ),
                drawStepBar(
                  icon: icon2,
                  txt: "DUE DATE",
                  isDots: true,
                  isBold: isBold2,
                  dotsLen: totalDots,
                ),
                drawStepBar(
                  icon: icon3,
                  txt: "BUDGET",
                  isDots: true,
                  isBold: isBold3,
                  dotsLen: totalDots,
                ),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(getHP(context, 10))),
    );
  }
}
