import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/AddTask3Screen.dart';
import 'package:aitl/view/dashboard/post_task/add/BaseAddTask.dart';
import 'package:aitl/view/widgets/DatePickerView.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view_model/api/dashboard/posttask/posttask_view_model.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mypkg/controller/classes/Common.dart';

class AddTask2Screen extends StatefulWidget {
  const AddTask2Screen({Key key}) : super(key: key);
  @override
  State createState() => _AddTask2ScreenState();
}

class _AddTask2ScreenState extends BaseAddTaskStatefullState<AddTask2Screen>
    with APIStateListener {
  String dueDate = "";

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.DelTaskAPIModel) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: TaskCancelType.onDelTask);
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.UserDeviceAPIModel) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      final date = new DateTime.now();
      dueDate = Common.getFormatedDate(
          format: 'dd-MMM-yyyy',
          mDate: DateTime(date.year, date.month, date.day + 15));
    } catch (e) {}

    try {
      MiscViewModel().fetchDataUserDevices(
        context: context,
        eventType: 'Task Post - Task Date',
      );
    } catch (e) {}
  }

  onNextClicked() {
    Get.to(
      () => AddTask3Screen(
        dueDate: dueDate,
      ),
    ).then(
      (value) {
        if (value != null) {
          Get.back(result: value);
        }
      },
    );
  }

  onDelTaskClicked() {
    try {
      PostTaskViewModel().fetchDataDeleteTask(context: context);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //log(taskController.getTaskModel().id.toString());

    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          title: 'Post Task',
          icon1: '01b',
          icon2: '02b',
          icon3: '03',
          isBold1: true,
          isBold2: true,
          isBold3: false,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: MMArrowBtn(
              txt: "Continue",
              icon: Icons.arrow_forward,
              height: getHP(context, 7),
              width: getW(context),
              callback: () {
                onNextClicked();
              },
            ),
          ),
          elevation: 0,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 1, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            DatePickerView(
              cap: 'When do you need it done?',
              isCapBold: false,
              capTxtColor: MyTheme.lgreyColor,
              topbotHeight: 10,
              dt: (dueDate == '') ? 'Due Date' : dueDate,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      dueDate =
                          DateFormat('dd-MMM-yyyy').format(value).toString();
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
