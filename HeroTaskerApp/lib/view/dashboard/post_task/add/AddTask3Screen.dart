import 'dart:developer';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/status/StatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/BaseAddTask.dart';
import 'package:aitl/view/widgets/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/StepperNum.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/dashboard/posttask/posttask_view_model.dart';
import 'package:aitl/view_model/api/misc/misc_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/AddTask3Controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:toggle_switch/toggle_switch.dart';

import 'AddTask4Screen.dart';

class AddTask3Screen extends StatefulWidget {
  final String dueDate;
  const AddTask3Screen({
    Key key,
    @required this.dueDate,
  }) : super(key: key);
  @override
  State createState() => _AddTask3ScreenState();
}

const minEstBudget = 50;
const maxEstBudget = 1000000;

class _AddTask3ScreenState extends BaseAddTaskStatefullState<AddTask3Screen>
    with APIStateListener {
  final estBudget = TextEditingController();
  final priceHr = TextEditingController();
  final totalHr = TextEditingController();

  bool isSwitchFixedPrice = true;
  int switchIndex = 0;
  int totalTaskers = 1;

  final AddTask3Controller c = Get.put(AddTask3Controller());

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.DelTaskAPIModel) {
        // log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: TaskCancelType.onDelTask);
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.PostTask3APIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              await rxCacheTaskModel(model.responseData.task);
              Get.to(() => AddTask4Screen(
                    dueDate: widget.dueDate,
                  )).then((value) {
                if (value != null) {
                  Get.back(result: value);
                }
              });
            } catch (e) {}
          }
        }
      } else if (apiType == APIType.UserDeviceAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      estBudget.dispose();
      priceHr.dispose();
      totalHr.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      MiscViewModel().fetchDataUserDevices(
        context: context,
        eventType: 'Task Post - Task Budget',
      );
    } catch (e) {}
  }

  validate() {
    if (isSwitchFixedPrice) {
      if (estBudget.text.isEmpty) {
        showToast(msg: 'Please enter estimated budget');
        return false;
      } else {
        if (double.parse(estBudget.text) < minEstBudget ||
            double.parse(estBudget.text) > maxEstBudget) {
          showToast(
              msg: 'Task allowed estimated price is ' +
                  minEstBudget.toString() +
                  ' - ' +
                  maxEstBudget.toString());
          return false;
        }
      }
    } else {
      if (priceHr.text.isEmpty) {
        showToast(msg: 'Please enter price per hour');
        return false;
      } else if (totalHr.text.isEmpty) {
        showToast(msg: 'Please enter hours');
        return false;
      }
    }

    return true;
  }

  calculation() {
    c.calculation(
      isSwitchFixedPrice: isSwitchFixedPrice,
      estBudget: estBudget,
      priceHr: priceHr,
      totalHr: totalHr,
    );
  }

  onNextClicked() {
    if (validate()) {
      final mEstBudget =
          estBudget.text == "" ? 0.0 : double.parse(estBudget.text.trim());
      final mPriceHr =
          priceHr.text == "" ? 0.0 : double.parse(priceHr.text.trim());
      final mtotalHr =
          totalHr.text == "" ? 0.0 : double.parse(totalHr.text.trim());

      PostTaskViewModel().fetchDataPutTask3(
        context: context,
        status: StatusCfg.TASK_STATUS_ACTIVE,
        dueDate: widget.dueDate,
        isFixedPrice: isSwitchFixedPrice,
        fixedBudgetAmount: mEstBudget,
        hourlyRate: mPriceHr,
        totalHours: mtotalHr,
        totalAmount: c.totalAmounts.toDouble(),
        workerNumber: totalTaskers,
      );
    }
  }

  onDelTaskClicked() {
    try {
      PostTaskViewModel().fetchDataDeleteTask(context: context);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          title: 'Post Task',
          icon1: '01b',
          icon2: '02b',
          icon3: '03b',
          isBold1: true,
          isBold2: true,
          isBold3: true,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: MMArrowBtn(
              txt: "Continue",
              icon: Icons.arrow_forward,
              height: getHP(context, 7),
              width: getW(context),
              callback: () {
                onNextClicked();
              },
            ),
          ),
          elevation: 0,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(height: 20),
            Center(
              child: ToggleSwitch(
                minWidth: getWP(context, 25),
                minHeight: getHP(context, 6.5),
                initialLabelIndex: switchIndex,
                cornerRadius: 50.0,
                fontSize: 17,
                activeFgColor: Colors.white,
                inactiveBgColor: HexColor.fromHex("#cad7dc"),
                inactiveFgColor: Colors.white,
                labels: ['Total', 'Hourly rate'],
                //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
                activeBgColor: HexColor.fromHex("#1caade"),
                onToggle: (index) {
                  switchIndex = index;
                  isSwitchFixedPrice = (index == 0) ? true : false;
                  calculation();
                  setState(() {});
                },
              ),
            ),

            /*Center(
              child: SwitchView(
                onTxt: "Total",
                offTxt: "Hourly rate",
                value: isSwitchFixedPrice,
                onChanged: (value) {
                  isSwitchFixedPrice = value;
                  if (mounted) {
                    setState(() {});
                  }
                  //callback((isSwitch) ? _companyName.text.trim() : '');
                },
              ),
            ),*/
            SizedBox(height: 40),
            (isSwitchFixedPrice) ? drawEstBudgetView() : drawPriceHrView(),

            //drawEstBudgetView(),
            // drawPriceHrView(),
            SizedBox(height: 40),
            drawTaskerNosView(),
          ],
        ),
      ),
    );
  }

  drawEstBudgetView() {
    if (!mounted) return;
    return InputTitleBoxHT(
      title: "What is your estimated budget (" +
          minEstBudget.toString() +
          "-" +
          maxEstBudget.toString() +
          ")?",
      ph: "",
      input: estBudget,
      kbType: TextInputType.number,
      len: 7,
      minLen: 0,
      prefixIco: getCurIcon(),
      onChange: calculation(),
    );
  }

  drawPriceHrView() {
    if (!mounted) return;
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Container(
              child: InputTitleBoxHT(
                title: "Price per hour",
                ph: "",
                input: priceHr,
                kbType: TextInputType.number,
                len: 7,
                minLen: 0,
                prefixIco: getCurIcon(),
                onChange: calculation(),
              ),
            ),
          ),
          SizedBox(width: 20),
          Flexible(
            child: Container(
              child: InputTitleBoxHT(
                title: "Hours",
                ph: "",
                input: totalHr,
                kbType: TextInputType.number,
                len: 2,
                minLen: 0,
                onChange: calculation(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawTaskerNosView() {
    return Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 6,
                child: Txt(
                    txt: "How many tasker do you need?",
                    txtColor: MyTheme.lgreyColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Flexible(
                //flex: 2,
                child: Txt(
                    txt: totalTaskers.toString(),
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Flexible(
                flex: 3,
                child: StepperNum(
                  count: totalTaskers,
                  callback: (val) {
                    totalTaskers = val;
                    calculation();
                    setState(() {});
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Flexible(
                child: Txt(
                    txt: "Estimated Budget:",
                    txtColor: MyTheme.lgreyColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              getCurIcon(),
              Obx(
                () => Txt(
                    txt: "${c.totalAmounts.toDouble().round()}",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Txt(
                  txt: (c.totalAmounts.toDouble().round() > 0 &&
                          totalTaskers > 1)
                      ? ' per Tasker'
                      : '',
                  txtColor: MyTheme.lgreyColor,
                  txtSize: MyTheme.txtSize + .3,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ],
      ),
    );
  }
}
