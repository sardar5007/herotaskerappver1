import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/services/PushNotificationService.dart';
import 'package:aitl/view/auth/AuthScreen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/misc/CaseAlertScreen.dart';
import 'package:aitl/view/splash/SplashScreen.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:cookie_jar/cookie_jar.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

      //SystemChrome.setSystemUIOverlayStyle(
      //SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      final pushNotificationService =
          PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(
          callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          Get.off(() => DashboardScreen()).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        FlutterRingtonePlayer.play(
          android: AndroidSounds.notification,
          ios: IosSounds.glass,
          looping: true, // Android only - API >= 28
          volume: 0.1, // Android only - API >= 28
          asAlarm: false, // Android only - all APIs
        );

        Get.to(() => CaseAlertScreen(
              message: message,
            )).then((value) async {
          await userData.setUserModel();
          Get.off(() => DashboardScreen()).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? SplashScreen()
                : Container(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(height: 40),
                          drawHeaderImage(),
                          SizedBox(height: 20),
                          drawCenterView(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 40),
                            child: MMArrowBtn(
                                txt: "Let's Start",
                                height: getHP(context, 7),
                                width: getW(context),
                                callback: () {
                                  Get.to(() => AuthScreen());
                                }),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),
          )),
    );
  }

  drawHeaderImage() {
    return Container(
      //width: getWP(context, 70),
      //height: getHP(context, 30),
      child: SvgPicture.asset(
        'assets/images/screens/welcome/welcome_theme.svg',
        //fit: BoxFit.fill,
      ),
    );
  }

  drawCenterView() {
    return Container(
        child: Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Txt(
            txt: "Welcome to the HeroTasker app",
            txtColor: MyTheme.timelineTitleColor,
            txtSize: MyTheme.txtSize + .2,
            txtAlign: TextAlign.center,
            isBold: true,
            //txtLineSpace: 1.5,
          ),
        ),
        Image.asset(
          "assets/images/screens/welcome/welcome_banner.png",
          fit: BoxFit.fitWidth,
          //height: getHP(context, 27),
          //width: getW(context),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Txt(
              txt:
                  "By using the HeroTasker app you can get many different types of work done.",
              txtColor: MyTheme.blueColor,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false
              //txtLineSpace: 1.5,
              ),
        ),
      ],
    ));
  }
}
