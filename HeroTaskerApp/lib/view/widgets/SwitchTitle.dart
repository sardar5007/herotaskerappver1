import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:toggle_switch/toggle_switch.dart';

class SwitchTitle extends StatefulWidget with Mixin {
  final txt;
  final Function(bool) callback;
  SwitchTitle({
    Key key,
    @required this.txt,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _SwitchTitleState();
}

class _SwitchTitleState extends State<SwitchTitle> with Mixin {
  int switchIndex = 1;

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Txt(
              txt: widget.txt,
              txtColor: MyTheme.lgreyColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
              //txtLineSpace: 1.5,
            ),
          ),
          //SizedBox(width: 5),
          ToggleSwitch(
            minWidth: getWP(context, 13),
            minHeight: getHP(context, 6),
            initialLabelIndex: switchIndex,
            cornerRadius: 50.0,
            activeFgColor: Colors.white,
            inactiveBgColor: HexColor.fromHex("#cad7dc"),
            inactiveFgColor: Colors.white,
            labels: ['Yes', 'No'],
            fontSize: 17,
            //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
            activeBgColor: HexColor.fromHex("#1caade"),
            onToggle: (index) {
              switchIndex = index;
              bool isSwitch = (index == 0) ? true : false;
              widget.callback(isSwitch);
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
