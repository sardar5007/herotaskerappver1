import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Function callback;

  const BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        child: new Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: false),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(10.0),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: 1.0, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
