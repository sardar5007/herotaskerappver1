import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class LoadingTxt extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Center(
        child: ScaleAnimatedTextKit(
          text: [
            "Loading...",
            "Please wait...",
          ],
          textStyle: TextStyle(
              fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
              color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
