import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:get/get.dart';

class StepperNum extends StatefulWidget {
  int count;
  final Function(int) callback;

  StepperNum({Key key, @required this.count, @required this.callback})
      : super(key: key);
  @override
  State createState() => _StepperNumState();
}

class _StepperNumState extends State<StepperNum> with Mixin {
  @override
  Widget build(BuildContext context) {
    double w = getWP(context, 22);
    double h = getHP(context, 6.5);
    return Container(
      width: w,
      height: h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50.0),
        color: MyTheme
            .brandColor, /*_circleAnimation.value == Alignment.centerLeft
                  ? Colors.grey
                  : MyTheme.redColor*/
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              widget.count--;
              if (widget.count < 1) widget.count = 1;
              widget.callback(widget.count);
            },
            child: Container(
              child: Txt(
                txt: "-",
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize + .5,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
          ),
          Container(
            color: Colors.white,
            width: 1,
            height: h,
          ),
          GestureDetector(
            onTap: () {
              widget.count++;
              widget.callback(widget.count);
            },
            child: Container(
              child: Txt(
                txt: "+",
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize + .5,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
