// to get places detail (lat/lng)
import 'package:aitl/config/google/GoogleAPIKey.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:mypkg/controller/Mixin.dart';
//  https://github.com/fluttercommunity/flutter_google_places/issues/140
//  https://stackoverflow.com/questions/55879550/how-to-fix-httpexception-connection-closed-before-full-header-was-received

class GPlacesView extends StatefulWidget {
  final String address;
  final String title;
  Color bgColor;
  final Function(String address, Location loc) callback;
  GPlacesView(
      {Key key,
      @required this.title,
      this.address = "",
      this.bgColor = Colors.transparent,
      @required this.callback})
      : super(key: key);
  @override
  _GPlacesViewState createState() => _GPlacesViewState();
}

class _GPlacesViewState extends State<GPlacesView> with Mixin {
  @override
  Widget build(BuildContext context) {
    final address = (widget.address == "") ? "Search" : widget.address;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (widget.title != null)
            ? Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Txt(
                    txt: widget.title,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              )
            : SizedBox(),
        new GestureDetector(
          onTap: () => _handlePressButton(),
          child: new Container(
            width: getW(context),
            //height: getHP(context, 9),
            //margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              color: widget.bgColor,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.grey, //
                width: 1,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading: Icon(
                  Icons.search,
                  color: Colors.black87,
                  size: 30,
                ),
                title: Txt(
                    txt: address,
                    txtColor: (widget.address == "Search")
                        ? MyTheme.lgreyColor
                        : Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage.toString());
    //homeScaffoldKey.currentState.showSnackBar(
    //SnackBar(content: Text(response.errorMessage)),
    //);
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: GoogleServerAPIKey.ApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      components: [Component(Component.country, "uk")],
    );
    if (p != null) {
      //displayPrediction(p, (String address, Location loc) {
      //widget.callback(address, loc);
      //});

      try {
        GoogleMapsPlaces _places = GoogleMapsPlaces(
          apiKey: GoogleServerAPIKey.ApiKey,
          //apiHeaders: await GoogleApiHeaders().getHeaders(),
        );
        if (_places != null) {
          PlacesDetailsResponse detail =
              await _places.getDetailsByPlaceId(p.placeId);
          String address = p.description;
          widget.callback(address, detail.result.geometry.location);
        }
      } catch (e) {
        log(e.toString());
      }
    }
  }
}
