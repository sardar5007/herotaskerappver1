import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class TxtBox extends StatelessWidget with Mixin {
  final txt;
  TextAlign txtAlign;
  double height;

  TxtBox(
      {Key key,
      @required this.txt,
      this.txtAlign = TextAlign.start,
      this.height = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: getHP(context, height),
      width: double.infinity,
      //color: Colors.white,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.grey),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 5),
            Txt(
              txt: txt,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }
}
