import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';
//  https://stackoverflow.com/questions/49273157/how-to-implement-drop-down-list-in-flutter

class DropDownPicker extends StatefulWidget {
  final String cap;
  final OptionItem itemSelected;
  final DropListModel dropListModel;
  final Color bgColor;
  Color txtColor;
  double txtSize;
  double ddTitleSize;
  double ddRadius;
  final bool isFullWidth;
  final Function(OptionItem optionItem) onOptionSelected;

  DropDownPicker({
    @required this.cap,
    @required this.itemSelected,
    @required this.dropListModel,
    @required this.onOptionSelected,
    this.bgColor = Colors.transparent,
    this.txtColor = Colors.black,
    this.txtSize,
    this.ddTitleSize = 0,
    this.ddRadius = 10,
    this.isFullWidth = false,
  }) {
    if (txtSize == null) {
      this.txtSize = MyTheme.txtSize;
    }
  }

  @override
  State createState() => _DropDownPickerState(itemSelected, dropListModel);
}

class _DropDownPickerState extends State<DropDownPicker>
    with SingleTickerProviderStateMixin, Mixin {
  OptionItem optionItemSelected;
  final DropListModel dropListModel;
  _DropDownPickerState(this.optionItemSelected, this.dropListModel);

  AnimationController expandController;
  Animation<double> animation;

  bool isShow = false;

  @override
  void initState() {
    super.initState();
    expandController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 0));
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
    _runExpandCheck();
  }

  void _runExpandCheck() {
    if (isShow) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  //@override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        this.isShow = !this.isShow;
        _runExpandCheck();
        setState(() {});
      },
      child: Container(
        //color: Colors.blue,
        //width: getWP(context, 80),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (widget.cap != null)
                ? Txt(
                    txt: widget.cap,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true)
                : SizedBox(),
            SizedBox(height: (widget.cap != null) ? 15 : 0),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(widget.ddRadius),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: widget.bgColor),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Txt(
                          txt: optionItemSelected.title,
                          txtColor: widget.txtColor,
                          txtSize: MyTheme.txtSize - widget.ddTitleSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ),
                  Align(
                    alignment: Alignment(1, 0),
                    child: Icon(
                      isShow
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down,
                      color: Colors.black,
                      size: 40,
                    ),
                  ),
                ],
              ),
            ),
            SizeTransition(
                axisAlignment: 1.0,
                sizeFactor: animation,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                      //width: 400,
                      // margin: const EdgeInsets.only(bottom: 10),
                      // padding: const EdgeInsets.only(bottom: 10),

                      child: _buildDropListOptions(
                          dropListModel.listOptionItems, context)),
                )),
//          Divider(color: Colors.grey.shade300, height: 1,)
          ],
        ),
      ),
    );
  }

  Column _buildDropListOptions(List<OptionItem> items, BuildContext context) {
    int i = 0;
    int len = items.length;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:
          items.map((item) => _buildSubMenu(item, context, len, i++)).toList(),
    );
  }

  Widget _buildSubMenu(
      OptionItem item, BuildContext context, int len, int index) {
    return GestureDetector(
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Txt(
                  txt: item.title,
                  txtColor: Colors.black,
                  txtSize: widget.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false,
                  isOverflow: true,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(color: Colors.grey, height: .5),
              )
            ],
          ),
        ),
      ),
      onTap: () {
        this.optionItemSelected = item;
        isShow = false;
        expandController.reverse();
        widget.onOptionSelected(item);
      },
    );
  }
}
