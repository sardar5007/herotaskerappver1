import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BtnOutlineIcon extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Color bgColor;
  final String leftIcon;
  final String rightIcon;
  final Color leftIconColor;
  final Color rightIconColor;
  final double radius;
  final Function callback;

  BtnOutlineIcon({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
    this.leftIcon,
    this.rightIcon,
    this.leftIconColor = Colors.grey,
    this.rightIconColor = Colors.grey,
    this.bgColor = Colors.transparent,
    this.radius = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        color: bgColor,
        child: ListTile(
          leading: (leftIcon != null)
              ? SvgPicture.asset(
                  leftIcon,
                  color: leftIconColor,
                )
              : SizedBox(),
          title: new Txt(
              txt: txt,
              txtColor: txtColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          trailing: (rightIcon != null)
              ? SvgPicture.asset(
                  rightIcon,
                  color: rightIconColor,
                )
              : SizedBox(),
        ),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(radius),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: 1.0, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
