import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mypkg/controller/Mixin.dart';

drawStepBar({
  bool isDots = false,
  String icon,
  String txt,
  Color txtColor = Colors.black,
  bool isBold = true,
  double dotsLen,
}) {
  return Container(
    //color: Colors.black,
    child: Row(
      children: [
        (isDots)
            ? Padding(
                padding: const EdgeInsets.only(right: 10),
                child: DottedLine(
                  direction: Axis.horizontal,
                  lineLength: dotsLen,
                  lineThickness: 2,
                  dashColor: Colors.grey,
                  dashGapLength: 0.5,
                ),
              )
            : SizedBox(),
        SvgPicture.asset(
          'assets/images/icons/svg/posttask/' + icon + '.svg',
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: isBold,
            isOverflow: true,
          ),
        ),
      ],
    ),
  );
}
