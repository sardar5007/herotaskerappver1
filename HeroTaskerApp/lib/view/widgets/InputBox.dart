import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class InputBox extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd;
  bool autofocus;
  InputBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    @required this.isPwd,
    this.autofocus = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: ctrl,
      autofocus: autofocus,
      keyboardType: kbType,
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : null,
      obscureText: isPwd,
      maxLength: len,
      autocorrect: false,
      style: TextStyle(
        color: Colors.black,
        fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        //hintText: lableTxt,
        labelText: lableTxt,
        /*hintStyle: new TextStyle(
          color: Colors.grey,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),*/
        labelStyle: new TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize + .3),
          height: MyTheme.txtLineSpace,
        ),
        //contentPadding: EdgeInsets.only(left: 20, right: 20),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.black,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: MyTheme.blueColor,
            width: 2,
          ),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.black,
            width: 1.5,
          ),
        ),
      ),
    );
  }
}
