import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';

import 'InputBox.dart';
import 'Txt.dart';

drawInputBox({
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required int len,
  String ph,
  bool isPwd = false,
  bool autofocus = false,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Txt(
          txt: title,
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.start,
          isBold: true),
      SizedBox(height: 15),
      InputBox(
        ctrl: input,
        lableTxt: (ph == null) ? title : ph,
        kbType: kbType,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
      ),
    ],
  );
}
