import 'dart:io';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class CamPicker with Mixin {
  List<String> _items = ['From Gallery', 'From Camera', 'Skip'];

  Future<File> _openCamera({isRear = true, String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  void showCamModal(
      {BuildContext context,
      String prefkey,
      Function callback,
      bool isRear = true}) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          color: Colors.white,
          padding: EdgeInsets.all(8),
          height: getHP(context, 30),
          alignment: Alignment.center,
          child: ListView.separated(
              itemCount: _items.length,
              separatorBuilder: (context, int) {
                return Divider(
                  color: Colors.grey,
                );
              },
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Column(
                    children: [
                      (index == 0)
                          ? Padding(
                              padding: const EdgeInsets.all(20),
                              child: Txt(
                                  txt: "Take a Pic",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize + .4,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                            )
                          : SizedBox(),
                      Card(
                        child: Txt(
                            txt: _items[index],
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      (index == 2) ? SizedBox(height: 20) : SizedBox()
                    ],
                  ),
                  onTap: () async {
                    Navigator.pop(context);
                    switch (index) {
                      case 0:
                        callback(await _openGallery(key: prefkey)); //  gallery
                        break;
                      case 1:
                        callback(await _openCamera(
                          key: prefkey,
                          isRear: isRear,
                        )); //  cam
                        break;
                      default:
                        break;
                    }
                  },
                );
              }),
        );
      },
    );
  }
}
