import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class InputBoxHT extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd, minLines, maxLines;
  Widget prefixIco;
  bool autofocus;
  final Function onChange;
  InputBoxHT({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    this.minLines,
    this.maxLines,
    @required this.isPwd,
    this.autofocus = false,
    this.prefixIco,
    this.onChange,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.black,
      child: TextField(
        controller: ctrl,
        autofocus: autofocus,
        keyboardType: kbType,
        minLines: minLines,
        maxLines: maxLines,
        onChanged: onChange,
        inputFormatters: (kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : null,
        obscureText: isPwd,
        maxLength: len,
        autocorrect: false,
        style: TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        decoration: new InputDecoration(
          //contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
          prefixIcon: (prefixIco != null)
              ? Padding(
                  padding: const EdgeInsets.all(10),
                  child: prefixIco,
                )
              : null,
          counterText: "",
          filled: true,
          fillColor: Colors.white,
          hintText: lableTxt,
          hintStyle: new TextStyle(
            color: MyTheme.lgreyColor,
            fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
            height: MyTheme.txtLineSpace,
          ),
          labelStyle: new TextStyle(
            color: Colors.black,
            fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
            height: MyTheme.txtLineSpace,
          ),
          contentPadding:
              EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MyTheme.lgreyColor),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: MyTheme.lgreyColor, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }
}
