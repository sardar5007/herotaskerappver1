import 'package:mypkg/controller/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BSBtn extends StatelessWidget with Mixin {
  final String txt;
  Color bgColor;
  double txtSize;
  double height;
  final double radius;
  final double leadingSpace;
  final bool isShowArrow;
  final Function callback;

  BSBtn({
    Key key,
    @required this.txt,
    this.bgColor,
    @required this.height,
    this.radius = 10,
    this.isShowArrow = true,
    @required this.callback,
    this.leadingSpace = 0,
    this.txtSize,
  }) {
    if (this.txtSize == null) this.txtSize = MyTheme.txtSize;
    if (this.bgColor == null) this.bgColor = MyTheme.redColor;
  }

  @override
  Widget build(BuildContext context) {
    if (this.height == null) height = getHP(context, 6);

    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        //width: width,
        height: height,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: this.bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: (isShowArrow)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: leadingSpace),
                  Flexible(
                    child: Txt(
                      txt: txt,
                      isOverflow: true,
                      txtColor: Colors.white,
                      txtSize: txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ),
                  SizedBox(width: (leadingSpace == 0) ? 20 : leadingSpace),
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.arrow_forward,
                      color: Colors.white,
                      size: 25,
                    ),
                  )
                ],
              )
            : Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Txt(
                  txt: txt,
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: true,
                ),
              ),
      ),
    );
  }
}
