import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/helper/utils/HelpTutHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import '../Txt.dart';

class HelpTutDialog extends StatefulWidget {
  @override
  State createState() => _HelpTutDialogState();
}

class _HelpTutDialogState extends State<HelpTutDialog> with Mixin {
  int index = 0;
  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    HelpTutHelper().listTips = null;
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: MyTheme.redColor,
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  iconSize: 30,
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Get.back();
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: Txt(
                txt: HelpTutHelper().listTips[index],
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: true,
                //txtLineSpace: 1.5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                color: Colors.black,
                height: getHP(context, 7),
                width: double.infinity,
                child: Row(
                  children: [
                    (index == 0)
                        ? SizedBox()
                        : Transform.translate(
                            offset: Offset(0, -10),
                            child: IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_left,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index--;
                                  _switchTabsHand(index);
                                });
                              },
                            ),
                          ),
                    Expanded(
                      child: Txt(
                        txt: HelpTutHelper().listTab[index].toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true,
                      ),
                    ),
                    (index == HelpTutHelper().listTips.length - 1)
                        ? SizedBox()
                        : Transform.translate(
                            offset: Offset(0, -10),
                            child: IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_right,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index++;
                                  _switchTabsHand(index);
                                });
                              },
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _switchTabsHand(int index2) {
    _stateProvider.notify(ObserverState.STATE_HELP_DIALOG_TAB, index2);
  }
}
