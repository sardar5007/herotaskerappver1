import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

showForgotDialog(
    {BuildContext context,
    TextEditingController email,
    Function callback}) async {
  await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: Colors.white,
            title: Txt(
                txt: "Password Reset",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Txt(
                      txt:
                          "Send an email to with instruction to reset your password?",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 20.0),
                  TextField(
                    controller: email,
                    keyboardType: TextInputType.emailAddress,
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize),
                    ),
                    decoration: InputDecoration(
                      labelText: 'Email/Phone',
                      labelStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      hintMaxLines: 1,
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                      ),
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.teal)),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Txt(
                            txt: 'Cancel',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        color: Colors.white,
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                          callback(email.text.trim());
                        },
                        child: Txt(
                            txt: 'Ok',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        color: Colors.white,
                      ),
                    ],
                  )
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)));
      });
}
