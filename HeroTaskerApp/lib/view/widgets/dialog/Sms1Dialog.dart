import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/AuthScreen.dart';
import 'package:aitl/view/auth/otp/Sms2Screen.dart';
import 'package:aitl/view/widgets/BtnOutlineIcon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import '../Txt.dart';
import 'package:get/get.dart';

class SMS1Dialog extends StatefulWidget {
  @override
  State createState() => SMS1DialogState();
}

class SMS1DialogState extends State<SMS1Dialog> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                    iconSize: 30,
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Get.back();
                    }),
              ),
              SizedBox(height: 10),
              BtnOutlineIcon(
                txt: "Log in with Mobile",
                txtColor: MyTheme.brandColor,
                borderColor: Colors.grey,
                bgColor: Colors.white,
                leftIcon: "assets/images/icons/svg/ico_phone.svg",
                leftIconColor: MyTheme.brandColor,
                radius: 50,
                callback: () {
                  Get.to(
                    () => Sms2Screen(),
                  ).then((value) {
                    Get.off(() => AuthScreen());
                  });
                },
              ),
              SizedBox(height: 20),
              Txt(
                txt: "Or continue with",
                txtColor: Colors.black54,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 20),
              BtnOutlineIcon(
                txt: "Log in with Facebook",
                txtColor: Colors.white,
                borderColor: Colors.transparent,
                bgColor: MyTheme.fbColor,
                leftIcon: "assets/images/icons/svg/ic_fb.svg",
                leftIconColor: Colors.white,
                radius: 50,
                callback: () {
                  showToast(msg: 'pending fb work', isToast: true);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
