import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'Txt.dart';
import 'package:get/get.dart';

class InputTitleBoxHT extends StatefulWidget {
  final String title;
  final TextEditingController input;
  final TextInputType kbType;
  final int len;
  final int minLen;
  final int minLine;
  final int maxLine;
  final String ph;
  final bool isPwd;
  final bool autofocus;
  final Function onChange;
  Widget prefixIco;
  InputTitleBoxHT({
    @required this.title,
    @required this.input,
    @required this.kbType,
    @required this.len,
    this.minLen = 0,
    this.minLine,
    this.maxLine,
    this.ph,
    this.isPwd = false,
    this.autofocus = false,
    this.prefixIco,
    this.onChange,
  });

  @override
  State createState() => _InputTitleBoHTState();
}

class _InputTitleBoHTState extends State<InputTitleBoxHT> {
  int count = 0;
  bool isOk = false;

  @override
  void initState() {
    super.initState();
    widget.input.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    count = widget.input.text.length;
    isOk = (count <= widget.minLen);

    var str = '';
    if (isOk)
      str = widget.minLen.toString() + "+";
    else
      str = widget.len.toString();

    return Container(
      // color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Txt(
                    txt: widget.title,
                    txtColor: MyTheme.lgreyColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              (widget.minLen > 0)
                  ? Txt(
                      txt: count.toString() + " / " + str,
                      txtColor: (isOk) ? Colors.red : Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false)
                  : SizedBox(),
            ],
          ),
          SizedBox(height: 15),
          InputBoxHT(
            ctrl: widget.input,
            lableTxt: (widget.ph == null) ? widget.title : widget.ph,
            kbType: widget.kbType,
            len: widget.len,
            isPwd: widget.isPwd,
            autofocus: widget.autofocus,
            maxLines: widget.maxLine,
            prefixIco: widget.prefixIco,
            onChange: widget.onChange,
          ),
        ],
      ),
    );
  }
}
