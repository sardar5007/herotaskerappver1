import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;
  Color txt1Color;
  Color txt2Color;

  TCView({
    @required this.screenName,
    this.txt1Color = Colors.black,
    this.txt2Color,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text:
                      "By clicking on 'Log in with Mobile' you confirm that you accept the ",
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: txt1Color,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize)),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'HeroTasker',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            color: (txt2Color == null)
                                ? MyTheme.blueColor
                                : txt2Color,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: ServerUrls.TC_URL,
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
