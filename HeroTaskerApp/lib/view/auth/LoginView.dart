import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/auth/otp/Sms1Screen.dart';
import 'package:aitl/view/auth/otp/Sms2Screen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ForgotDialog.dart';
import 'package:aitl/view_model/api/auth/forgot_view_model.dart';
import 'package:aitl/view_model/api/auth/login_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/profile/ProfileHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/form_validator/UserProfileVal.dart';

class LoginView extends StatefulWidget {
  @override
  State createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with APIStateListener, Mixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.LoginAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              try {
                await DBMgr.shared
                    .setUserProfile(user: model.responseData.user);
                await userData.setUserModel();
              } catch (e) {
                log(e.toString());
              }
              try {
                ProfileHelper().downloadProfileImages(
                  context: context,
                );
              } catch (e) {
                log(e.toString());
              }
              if (Server.isTest) {
                Get.off(() => DashboardScreen());
              } else {
                if (model.responseData.user.isMobileNumberVerified) {
                  Get.off(() => DashboardScreen());
                } else {
                  Get.to(() => Sms2Screen(
                        mobile: '',
                      )).then((value) {
                    //callback(route);
                  });
                }
              }
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              if (mounted) {
                //final err = model.errorMessages.login[0].toString();
                showToast(msg: "You have entered invalid credentials");
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiType == APIType.ForgotAPIModel) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final msg = model.messages.forgotpassword[0].toString();
              showToast(msg: msg, which: 1);
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              if (mounted) {
                final err = model.errorMessages.forgotpassword[0].toString();
                showToast(msg: err);
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text =
            "mesuk1@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _pwd.text = "123";
      }
    } catch (e) {}

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal().isEmpty(_email, 'Email/Phone', MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 40, right: 40),
                  child: Column(
                    children: [
                      AuthHelper().drawAuthHeaderImage(context),
                      InputBox(
                        ctrl: _email,
                        lableTxt: "Email or Phone Number",
                        kbType: TextInputType.emailAddress,
                        len: 50,
                        isPwd: false,
                      ),
                      InputBox(
                        ctrl: _pwd,
                        lableTxt: "Password",
                        kbType: TextInputType.text,
                        len: 20,
                        isPwd: true,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                              onTap: () {
                                showForgotDialog(
                                    context: context,
                                    email: _email,
                                    callback: (String emailStr) {
                                      //
                                      if (emailStr != null) {
                                        if (emailStr.length > 0) {
                                          ForgotViewModel().fetchData(
                                            context: context,
                                            email: _email.text.trim(),
                                          );
                                        } else {
                                          showToast(msg: "Missing email");
                                        }
                                      }
                                    });
                              },
                              child: Txt(
                                  txt: "Forgot Password ?",
                                  txtColor: MyTheme.blueColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false)),
                          SizedBox(width: 10),
                          MMBtn(
                            txt: "Login",
                            //txtColor: Colors.white,
                            //bgColor: accentColor,
                            width: getWP(context, 30),
                            height: getHP(context, 6),
                            radius: 20,
                            callback: () {
                              if (validate()) {
                                LoginViewModel().fetchData(
                                  context: context,
                                  email: _email.text.trim(),
                                  pwd: _pwd.text.trim(),
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Txt(
                txt: "Or continue with",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: MMBtn(
                  txt: "Log in with Mobile",
                  width: getW(context),
                  height: getHP(context, 7),
                  radius: 10,
                  callback: () async {
                    Get.to(
                      () => Sms1Screen(),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(40),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, 7),
                      icon: "ic_google",
                      callback: () {
                        showToast(msg: 'pending work', isToast: true);
                      },
                    ),
                    SizedBox(width: getWP(context, 7)),
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, 7),
                      icon: "ic_fb",
                      callback: () {
                        showToast(msg: 'pending work', isToast: true);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: getHP(context, 5)),
            ],
          ),
        ),
      ),
    );
  }
}
