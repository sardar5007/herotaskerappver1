import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/auth/otp/Sms2Screen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/widgets/DatePickerView.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view_model/api/auth/profile/reg_profile_view_model.dart';
import 'package:aitl/view_model/helper/auth/profile/ProfileHelper.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/form_validator/UserProfileVal.dart';
import 'package:get/get.dart';

class RegView2 extends StatefulWidget {
  @override
  State createState() => _RegView2State();
}

enum genderEnum { male, female }

class _RegView2State extends State<RegView2> with APIStateListener, Mixin {
  final _area = TextEditingController();

  //  reg2
  String dob = "";
  String regAddr = "";
  Location loc;
  genderEnum _gender = genderEnum.male;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.RegProfileAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
              );
              await userData.setUserModel();
            } catch (e) {
              log(e.toString());
            }
            try {
              ProfileHelper().downloadProfileImages(
                context: context,
              );
            } catch (e) {
              log(e.toString());
            }

            if (model.responseData.user.isMobileNumberVerified) {
              Get.off(() => DashboardScreen());
            } else {
              Get.to(
                () => Sms2Screen(
                  mobile: userData.userModel.mobileNumber,
                ),
              ).then((value) {
                //callback(route);
              });
            }
          } else {
            try {
              if (mounted) {
                final err = model.messages.postUser[0].toString();
                showToast(msg: err);
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _area.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isDOBOK(dob, MyTheme.redColor)) {
      return false;
    } else if (regAddr == "") {
      showToast(msg: "Please search your address");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return SafeArea(
      //  ******** On RegView2
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'Create profile',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: Container(
          width: getW(context),
          height: getH(context),
          color: Colors.white,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: getHP(context, 5)),
                  //drawCompSwitch(),
                  Txt(
                      txt: "Provide a date of birth",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 5),
                  Txt(
                      txt:
                          "To signup, you must be 18 or older. Other people won't see your birthday.",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),
                  DatePickerView(
                    cap: null,
                    dt: (dob == '') ? 'Select a date' : dob,
                    initialDate: dateDOBlast,
                    firstDate: dateDOBfirst,
                    lastDate: dateDOBlast,
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat('dd-MMM-yyyy')
                                .format(value)
                                .toString();
                          } catch (e) {
                            log(e.toString());
                          }
                        });
                      }
                    },
                  ),
                  SizedBox(height: 30),
                  drawGender(),
                  InputBox(
                    ctrl: _area,
                    lableTxt: "Enter your area",
                    kbType: TextInputType.streetAddress,
                    len: 255,
                    isPwd: false,
                  ),
                  GPlacesView(
                      title: null,
                      address: regAddr,
                      callback: (String address, Location loc) {
                        regAddr = address;
                        this.loc = loc;
                        setState(() {});
                      }),
                  SizedBox(height: 30),
                  MMBtn(
                    txt: "Get Started",
                    width: getW(context),
                    height: getHP(context, 7),
                    radius: 20,
                    callback: () {
                      if (validate()) {
                        RegProfileViewModel().fetchData(
                          context: context,
                          address: (_area.toString().trim() + ' ' + regAddr)
                              .toString()
                              .trim(),
                          briefBio: '',
                          communityId: userData.communityId,
                          dob: dob,
                          email: userData.userModel.email,
                          fname: userData.userModel.firstName,
                          cohort:
                              (_gender == genderEnum.male) ? "Male" : "Female",
                          headline: '',
                          lname: userData.userModel.lastName,
                          lat: loc.lat,
                          lng: loc.lng,
                          mobile: userData.userModel.mobileNumber,
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.black,
              disabledColor: Colors.black,
              selectedRowColor: Colors.black,
              indicatorColor: Colors.black,
              toggleableActiveColor: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
