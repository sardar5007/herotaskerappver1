import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/auth/login_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/TCView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/auth/reg_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/profile/ProfileHelper.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/form_validator/UserProfileVal.dart';
import 'RegView2.dart';
import 'package:get/get.dart';

class RegView extends StatefulWidget {
  @override
  State createState() => _RegViewState();
}

enum genderEnum { male, female }

class _RegViewState extends State<RegView> with APIStateListener, Mixin {
  //  reg1
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.LoginAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model.success) {
            //log(model.responseData.user.address);
            try {
              //DBMgr.shared.getUserProfile();
              try {
                await DBMgr.shared.setUserProfile(
                  user: model.responseData.user,
                  otpID: model.responseData.otpId.toString(),
                  otpMobileNumber: model.responseData.otpMobileNumber,
                );
                await userData.setUserModel();
              } catch (e) {
                log(e.toString());
              }
              try {
                ProfileHelper().downloadProfileImages(
                  context: context,
                );
              } catch (e) {
                log(e.toString());
              }

              Get.off(
                () => RegView2(),
              ).then((value) {
                //callback(route);
              });
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              if (mounted) {
                //final err =
                //model2.errorMessages.login[0].toString();
                showToast(msg: "You have entered invalid credentials");
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiType == APIType.RegAPIModel) {
        //log(model);
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie
              LoginViewModel().fetchData(
                context: context,
                email: _email.text.trim(),
                pwd: _pwd.text.trim(),
              );
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.register[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isFNameOK(_fname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(_email, "Invalid email address", MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_mobile, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getHP(context, 5)),
            AuthHelper().drawAuthHeaderImage(context),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 40, right: 40),
                child: Column(
                  children: [
                    InputBox(
                      ctrl: _fname,
                      lableTxt: "First name",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _lname,
                      lableTxt: "Last name",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _email,
                      lableTxt: "Email",
                      kbType: TextInputType.emailAddress,
                      len: 50,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _mobile,
                      lableTxt: "Please enter your phone number",
                      kbType: TextInputType.phone,
                      len: 20,
                      isPwd: false,
                    ),
                    InputBox(
                      ctrl: _pwd,
                      lableTxt: "Password",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                    SizedBox(height: 10),
                    MMBtn(
                      txt: "Create an account",
                      width: getW(context),
                      height: getHP(context, 7),
                      radius: 20,
                      callback: () async {
                        if (validate()) {
                          RegViewModel().fetchData(
                            context: context,
                            email: _email.text.trim(),
                            pwd: _pwd.text.trim(),
                            fname: _fname.text.trim(),
                            lname: _lname.text.trim(),
                            mobile: _mobile.text.trim(),
                            dob: "",
                            gender: "",
                            address: "",
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Txt(
              txt: "Or continue with",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Padding(
              padding: const EdgeInsets.all(40),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, 7),
                    icon: "ic_google",
                    callback: () {
                      showToast(msg: 'pending work', isToast: true);
                    },
                  ),
                  SizedBox(width: getWP(context, 7)),
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, 7),
                    icon: "ic_fb",
                    callback: () {
                      showToast(msg: 'pending work', isToast: true);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
              child: Center(child: TCView(screenName: 'Continue')),
            ),
            SizedBox(height: getHP(context, 5)),
          ],
        ),
      ),
    );
  }
}
