import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/LoginView.dart';
import 'package:aitl/view/auth/RegView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

class AuthScreen extends StatefulWidget {
  bool isSwitch = true;
  String fname = '';
  String lname = '';
  String email = '';
  String mobile = '';
  String pwd = '';
  String compName = '';

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            title: Txt(
                txt: 'Join HeroTasker',
                txtColor: Colors.black,
                txtSize: MyTheme.appbarTitleFontSize,
                txtAlign: TextAlign.center,
                isBold: false),
            centerTitle: false,
            iconTheme:
                IconThemeData(color: Colors.black //change your color here
                    ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 7)),
              child: Container(
                color: MyTheme.brandColor,
                height: getHP(context, 7),
                child: TabBar(
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 5,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, 7),
                        child: Txt(
                      txt: "CREATE AN ACCOUNT",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "LOG IN",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              children: [
                RegView(),
                LoginView(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
