import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/api/auth/otp/sms2_view_model.dart';
import 'package:mypkg/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:get/get.dart';

class Sms2Screen extends StatefulWidget {
  final String mobile;
  Sms2Screen({Key key, this.mobile = ''}) : super(key: key);
  @override
  State createState() => _Sms2ScreenState();
}

class _Sms2ScreenState extends State<Sms2Screen> with APIStateListener, Mixin {
  final TextEditingController _phoneController = TextEditingController();

  Color btnBgColor;
  Color btnTxtColor;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(state, apiType, model) async {
    try {
      if (apiType == APIType.MobileUserOtpPostAPIModel) {
        //log(model);
        if (model != null && mounted) {
          if (model is MobileUserOtpPostAPIModel) {
            try {
              final err = model.messages.postUserotp[0].toString();
              showToast(msg: err);
            } catch (e) {}
          } else {
            try {
              if (model.success) {
                try {
                  //final msg = model.messages.postUserotp[0].toString();
                  //showToast(msg: msg, which: 1);
                  Get.to(
                    () => Sms3Screen(
                      mobileUserOTPModel: model.responseData.userOTP,
                    ),
                  ).then((value) {
                    //callback(route);
                  });
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.messages.postUserotp[0].toString();
                    showToast(msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _phoneController.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.mobile.length == 0) {
        btnBgColor = Colors.grey[300];
        btnTxtColor = Colors.black;
      } else {
        btnBgColor = MyTheme.redColor;
        btnTxtColor = Colors.white;
      }

      _phoneController.text = widget.mobile;
      _phoneController.addListener(() {
        if (mounted) {
          if (_phoneController.text.trim().length <
              UserProfileVal.PHONE_LIMIT) {
            btnBgColor = Colors.grey[300];
            btnTxtColor = Colors.black;
          } else {
            btnBgColor = MyTheme.redColor;
            btnTxtColor = Colors.white;
          }
          setState(() {});
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: MyTheme.bgColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Form(
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 5)),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Enter your mobile number",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              //txtLineSpace: 1.0,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              controller: _phoneController,
              keyboardType: TextInputType.phone,
              maxLength: 15,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize:
                    getTxtSize(context: context, txtSize: MyTheme.txtSize + .3),
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                hintStyle: new TextStyle(
                  color: Colors.grey,
                  fontSize: getTxtSize(
                      context: context, txtSize: MyTheme.txtSize + .8),
                ),
                hintText: AppDefine.COUNTRY_CODE + " xxxxxx",
                //prefixText: AppDefine.COUNTRY_CODE,
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                border: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          MMBtn(
            txt: "Next",
            txtColor: btnTxtColor,
            bgColor: btnBgColor,
            width: getWP(context, 90),
            height: getHP(context, 8),
            radius: 10,
            callback: () async {
              if (_phoneController.text.trim().length >=
                  UserProfileVal.PHONE_LIMIT) {
                SMS2ViewModel().fetchData(
                  context: context,
                  mobile: _phoneController.text.trim(),
                  userId:
                      (userData.userModel != null) ? userData.userModel.id : 0,
                );
              } else {
                showToast(msg: 'Please enter your valid phone number');
              }
            },
          ),
        ],
      ),
    );
  }
}
