import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/AuthScreen.dart';
import 'package:aitl/view/widgets/IcoTxtIcoMM.dart';
import 'package:aitl/view/widgets/TCView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/Sms1Dialog.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:get/get.dart';

class Sms1Screen extends StatefulWidget {
  @override
  State createState() => _Sms1ScreenState();
}

class _Sms1ScreenState extends State<Sms1Screen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    /*try {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
            statusBarColor: MyTheme.redColor,
            statusBarIconBrightness: Brightness.dark),
      );
    } catch (e) {}*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.brandColor,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: MyTheme.bgColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return ListView(
      shrinkWrap: true,
      children: [
        SizedBox(height: getHP(context, 10)),
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40, bottom: 20),
          child: Txt(
            txt: "Please give your mobile number to start using the app",
            txtColor: Colors.white,
            txtSize: MyTheme.txtSize + .5,
            txtAlign: TextAlign.center,
            //txtLineSpace: 1.2,
            isBold: false,
          ),
        ),
        drawCenterMobileCode(),
        Padding(
          padding: const EdgeInsets.only(top: 40, left: 40, right: 20),
          child: TCView(
            screenName: 'Log in',
            txt1Color: Colors.white,
            txt2Color: Colors.white,
          ),
        ),
      ],
    );
  }

  drawCenterMobileCode() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: getHP(context, 45),
        width: getWP(context, 80),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(20.0),
            topRight: const Radius.circular(20.0),
            bottomLeft: const Radius.circular(20.0),
            bottomRight: const Radius.circular(20.0),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                txt: "Please give your mobile number",
                txtColor: MyTheme.blueColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
            GestureDetector(
              onTap: () async {
                Get.dialog(SMS1Dialog()).then((value) {});
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: IcoTxtIcoMM(
                  leftIcon: Icons.mobile_screen_share,
                  txt: "Log in with Mobile",
                  rightIcon: Icons.arrow_right,
                  iconColor: MyTheme.blueColor,
                  leftIconSize: 40,
                  rightIconSize: 50,
                ),
              ),
            ),
            SizedBox(height: getHP(context, 5)),
            Expanded(
              child: Container(
                width: getWP(context, 80),
                //height: getHP(context, 20),
                child: Image.asset(
                  'assets/images/screens/login/login_mobile_bg.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
