import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double appbarTitleFontSize = 2.5;
  static final double txtSize = 2;
  static final String fontFamily = "MuseoSans500";
  static final double appbarElevation = 1;
  static final double txtLineSpace = 1.3;

  static final Color bgColor = HexColor.fromHex("#f7f7f7");
  //static final Color bgPostTaskColor = HexColor.fromHex("#C4C4C4");

  static final Color blueColor = HexColor.fromHex("#1caade");
  static final Color redColor = HexColor.fromHex("#E73458");
  static final Color pinkColor = HexColor.fromHex("#FFA9AC");
  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color gray1Color = HexColor.fromHex("#f5f9fb");
  static final Color gray2Color = HexColor.fromHex("#e7eef1");
  static final Color gray3Color = HexColor.fromHex("#cad7dc");
  static final Color gray4Color = HexColor.fromHex("#839094");
  static final Color gray5Color = HexColor.fromHex("#3a3d3e");
  static final Color lgreyColor = HexColor.fromHex("#839094");
  static final Color dgreyColor = HexColor.fromHex("#FFA9AC");
  static final Color appbarProgColor = Colors.orangeAccent;
  static final Color appbarTxtColor = Colors.black87;
  static final Color btnColor = HexColor.fromHex("#E73458");

  static final Color brandColor = HexColor.fromHex("#1caade");
  static final Color airBlueColor = HexColor.fromHex("#008fb4");
  static final Color airBlueMidColor = HexColor.fromHex("#186b8e");
  static final Color airGreenColor = HexColor.fromHex("#9fc104");
  static final Color dGreenColor = HexColor.fromHex("#2da346");
  static final Color fbColor = HexColor.fromHex("#3B5998");

  //  pages color theme

  // dashboard->post task theme
  static final Color titlePostTaskColor = HexColor.fromHex("#61000000");
  static final Color iconTextColor = HexColor.fromHex("#839094");

  // mycases
  static final Color mycasesNFBtnColor = HexColor.fromHex("#B4B4B4");

  //  timeline
  static final Color timelineTitleColor = HexColor.fromHex("#1F3548");
  static final Color timelinePostCallerNameColor = HexColor.fromHex("#2D2D2D");

  //  more
  static final Color moreTxtColor = HexColor.fromHex("#1F3548");

  //  lead
  static final Color leadSubTitle = HexColor.fromHex("#151522");

  //  theme data
  static final ThemeData refreshIndicatorTheme =
      ThemeData(canvasColor: gray5Color, accentColor: Colors.white);

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: MyTheme.brandColor,
    disabledColor: MyTheme.brandColor,
    selectedRowColor: MyTheme.brandColor,
    indicatorColor: MyTheme.brandColor,
    toggleableActiveColor: MyTheme.brandColor,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#ffffff"),
    //iconTheme: IconThemeData(color: Colors.red),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  //static final BoxDecoration boxDeco = BoxDecoration(
  //color: Colors.white,
  //);

  static final boxDeco = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);

  static final picEmboseCircleDeco =
      BoxDecoration(color: Colors.grey, shape: BoxShape.circle, boxShadow: [
    BoxShadow(
      color: Colors.grey.shade500,
      blurRadius: 5.0,
      spreadRadius: 2.0,
    ),
  ]);
}
