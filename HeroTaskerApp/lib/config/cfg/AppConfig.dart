class AppConfig {
  //static const
  static const int page_limit_dashboard = 3;
  static const int page_limit = 10;

  static const double picSize = 400;
  static const AlertDismisSec = 5;
}
