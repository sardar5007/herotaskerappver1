class AppDefine {
  //  DEFINE STUFF START HERE...
  static const APP_NAME = "MortgageMagic";
  static const SUPPORT_EMAIL = "info@mortgage-magic.co.uk";
  static const SUPPORT_CALL = "0333 888 0245";
  static const COUNTRY_CODE = "+44";
  static const String CUR_SIGN = "€";
  static const geo_code = "uk";

  static const ORDINAL_NOS = [
    '1st',
    '2nd',
    '3rd',
    '4th',
    '5th',
    '6th',
    '7th',
    '8th',
    '9th',
    '10th',
    '11th',
    '12th',
    '13th',
    '14th',
    '15th',
    '16th',
    '17th',
    '18th',
    '19th',
    '20th',
    '21st',
    '22nd',
    '23rd',
    '24th',
    '25th',
    '26th',
    '27th',
    '28th',
    '29th',
    '30th',
    '31st'
  ];
}
