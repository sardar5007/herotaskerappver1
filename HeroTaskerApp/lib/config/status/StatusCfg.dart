class StatusCfg {
  // Task Status
  static const int TASK_STATUS_DRAFT = 903;
  static const int TASK_STATUS_ACTIVE = 101;
  static const int TASK_STATUS_ACCEPTED = 102;
  static const int TASK_STATUS_PAYMENTED = 103;
  static const int TASK_STATUS_CANCELLED = 104;
  static const int TASK_STATUS_DELETED = 105;
  static const int TASK_STATUS_ALL = 901;
  static const int TASK_STATUS_PENDINGTASK = 902;
  static const int STATUS_ALL_PRIVATEMESSAGE = 904;

  String getSatus(int status) {
    switch (status) {
      case 1:
        return "Receipt";
      case 2:
        return "Payment";
      case 3:
        return "POST";
      case 4:
        return "PURCHASE ORDER";
      case 101:
        return "ACTIVE";
      case 102:
        return "ASSIGNED";
      case 103:
        return "PAID";
      case 104:
        return "CANCELLED";
      case 105:
        return "DELETED";
      case 901:
        return "ALLTASK";
      case 902:
        return "PENDINGTASK";
      case 903:
        return "DRAFT";
      case 904:
        return "PRIVATEMESSAGE";
      case 201:
        return "REQUESTPAYMENT";
      case 202:
        return "RECEIVEDPAYMENT";
      case 203:
        return "FUNDED";
      case 204:
        return "CASH";
      case 205:
        return "FUNDED_REQUESTPAYMENT";
      case 206:
        return "FUNDED_RELEASEPAYMENT";
      case 905:
        return "VERIFIED";
      case 906:
        return "UNVERIFIED";
      default:
        return "";
    }
  }
}
