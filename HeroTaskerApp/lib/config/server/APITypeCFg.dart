enum APIType {
  LoginAPIModel,
  ForgotAPIModel,
  RegAPIModel,
  RegProfileAPIModel,
  MobileUserOtpPostAPIModel,
  MobileUserOtpPutAPIModel,
  FcmDeviceInfoAPIModel,
  UserDeviceAPIModel,
  PendingReviewRatingByUserIdAPIModel,
  UserRatingSummaryAPIModel,
  TaskInfoSearchAPIModel,
  MediaUploadFilesAPIModel,
  PostTask1APIModel,
  PostTask3APIModel,
  EmailNoti,
  ParentAllCatAPIModel,
  ChildAllCatAPIModel,
  GetPicAPIModel,
  SavePicAPIModel,
  DelPicAPIModel,
  DelTaskAPIModel,
}
