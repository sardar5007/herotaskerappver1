import 'package:aitl/config/server/Server.dart';

class APIMyTasksCfg {
  //  task details url
  static const String GET_TIMELINE_URL =
      Server.BASE_URL + "/api/timeline/gettimelinebyapp";

  static const String GET_TASKBIDDING_URL =
      Server.BASE_URL + "/api/taskbidding/get";
}
