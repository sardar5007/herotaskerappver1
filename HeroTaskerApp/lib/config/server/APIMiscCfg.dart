import 'Server.dart';

class APIMiscCfg {
  //  Device Info & APNS Stuff
  static const String FCM_DEVICE_INFO_URL =
      Server.BASE_URL + "/api/fcmdeviceinfo/post";

  //  More::Help->Support->Send Email  + Attachments
  static const String MEDIA_UPLOADFILES_URL =
      Server.BASE_URL + "/api/media/uploadpictures";

  static const String USER_DEVICES_POST_URL =
      Server.BASE_URL + "/api/userdevice/post";
}
