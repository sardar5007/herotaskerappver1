import 'package:get/get.dart';

class BottomTabbarController extends GetxController {
  var selectedItem = 0.obs;
  setSelectedIndex({var selectValue}) {
    selectedItem.value = selectValue;
  }
}
