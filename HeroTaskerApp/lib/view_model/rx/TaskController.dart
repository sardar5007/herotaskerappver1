import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:get/get.dart';

class TaskController extends GetxController {
  var _taskModel = TaskModel().obs;
  var taskImages = RxList<Map<String, dynamic>>().obs;
  setTaskModel(TaskModel model) {
    _taskModel = model.obs;
  }

  TaskModel getTaskModel() {
    return _taskModel.value;
  }
}
