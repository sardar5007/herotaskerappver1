import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/classes/Common.dart';
import 'package:get/get.dart';

class PostTaskViewModel<T> with Mixin {
  final TaskController taskController = Get.put(TaskController());

  //  Post Task 1 start here..
  Future<void> fetchDataPostTask1({
    BuildContext context,
    int status,
    String taskHeadline,
    String taskDesc,
    String taskLoc,
    Location taskCord,
    String taskAddr,
    bool isOnlineJob,
  }) async {
    try {
      await NetworkMgr().req<PostTask1APIModel, Null>(
        context: context,
        url: APIPostTaskCfg.POSTTASK1_URL,
        param: {
          "DeliveryDate": Common.getFormatedDate(format: "dd-MMM-yyyy"),
          "DeliveryTime": "",
          "Description": taskDesc,
          "DueAmount": 0.0,
          "DutDateType": 0,
          "EmployeeId": 0,
          "FixedBudgetAmount": 0.0,
          "HourlyRate": 0.0,
          "IsFixedPrice": false,
          "IsInPersonOrOnline": false,
          "JobCategory": "",
          "Latitude": (taskCord != null) ? taskCord.lat : 0.0,
          "Longitude": (taskCord != null) ? taskCord.lng : 0.0,
          "NetTotalAmount": 0.0,
          "PaidAmount": 0.0,
          "PreferedLocation": (taskLoc + ' ' + taskAddr).trim(),
          "Requirements": "",
          "Skill": "",
          "Status": status,
          "Id": 0,
          "Title": taskHeadline,
          "TotalBidsNumber": 0,
          "TotalHours": 0.0,
          "UserId": userData.userModel.id,
          "WorkerNumber": 0,
        },
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.PostTask1APIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> fetchDataPutTask1({
    BuildContext context,
    int status,
    int taskId,
    String taskHeadline,
    String taskDesc,
    String taskLoc,
    String taskAddr,
    Location taskCord,
    bool isOnlineJob,
    bool isFixedPrice = false,
    double fixedBudgetAmount = 0.0,
    double hourlyRate = 0.0,
    double totalHours = 0.0,
    double totalAmount = 0.0,
  }) async {
    try {
      await NetworkMgr().req<PostTask1APIModel, Null>(
        context: context,
        url: APIPostTaskCfg.POSTTASK1_PUT_URL,
        reqType: ReqType.Put,
        param: {
          "DeliveryDate": Common.getFormatedDate(format: "dd-MMM-yyyy"),
          "DeliveryTime": "",
          "Description": taskDesc,
          "DueAmount": 0.0,
          "DutDateType": 0,
          "EmployeeId": 0,
          "FixedBudgetAmount": fixedBudgetAmount,
          "HourlyRate": hourlyRate,
          "IsFixedPrice": isFixedPrice,
          "IsInPersonOrOnline": false,
          "JobCategory": "",
          "Latitude": (taskCord != null) ? taskCord.lat : 0.0,
          "Longitude": (taskCord != null) ? taskCord.lng : 0.0,
          "NetTotalAmount": totalAmount,
          "PaidAmount": 0.0,
          "PreferedLocation": (taskLoc + ' ' + taskAddr).trim(),
          "Requirements": "",
          "Skill": "",
          "Status": status,
          "Id": taskId,
          "Title": taskHeadline,
          "TotalBidsNumber": 0,
          "TotalHours": totalHours,
          "UserId": userData.userModel.id,
          "WorkerNumber": 0,
        },
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.PostTask1APIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
  //  Post Task 1 end here..
  //  Post Task 3 start here..

  Future<void> fetchDataPutTask3({
    BuildContext context,
    int status,
    String dueDate,
    bool isFixedPrice = false,
    double fixedBudgetAmount = 0.0,
    double hourlyRate = 0.0,
    double totalHours = 0.0,
    double totalAmount = 0.0,
    int workerNumber = 1,
  }) async {
    try {
      await NetworkMgr().req<PostTask1APIModel, Null>(
        context: context,
        url: APIPostTaskCfg.POSTTASK1_PUT_URL,
        reqType: ReqType.Put,
        param: {
          "DeliveryDate": dueDate,
          "DeliveryTime": "",
          "Description": taskController.getTaskModel().description,
          "DueAmount": 0.0,
          "DutDateType": 0,
          "EmployeeId": 0,
          "FixedBudgetAmount": fixedBudgetAmount,
          "HourlyRate": hourlyRate,
          "IsFixedPrice": isFixedPrice,
          "IsInPersonOrOnline":
              taskController.getTaskModel().isInPersonOrOnline,
          "JobCategory": "",
          "Latitude": taskController.getTaskModel().latitude ?? 0.0,
          "Longitude": taskController.getTaskModel().longitude ?? 0.0,
          "NetTotalAmount": totalAmount,
          "PaidAmount": 0.0,
          "PreferedLocation":
              taskController.getTaskModel().preferedLocation ?? '',
          "Requirements": "",
          "Skill": "",
          "Status": status,
          "Id": taskController.getTaskModel().id,
          "Title": taskController.getTaskModel().title,
          "TotalBidsNumber": 0,
          "TotalHours": totalHours,
          "UserId": userData.userModel.id,
          "WorkerNumber": workerNumber,
        },
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.PostTask3APIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
  //  Post Task 3 end here..

  //  Post Task 1 start here..
  Future<void> fetchDataDeleteTask({
    BuildContext context,
  }) async {
    try {
      await NetworkMgr()
          .req<PostTask1APIModel, Null>(
        context: context,
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.PostTask1APIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
