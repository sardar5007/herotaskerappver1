import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentAllCatAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:mypkg/controller/classes/Common.dart';

class AllCatViewModel<T> with Mixin {
  Future<void> fetchDataParentCat({
    BuildContext context,
    int parentId,
  }) async {
    try {
      await NetworkMgr()
          .req<ParentAllCatAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.PARENT_TASK_CAT_GET_URL,
        param: {"parentId": parentId},
        reqType: ReqType.Get,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.ParentAllCatAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> fetchDataChildCat({
    BuildContext context,
    int parentId,
  }) async {
    try {
      await NetworkMgr()
          .req<ChildAllCatAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.CHILD_TASK_CAT_GET_URL,
        param: {"parentId": parentId},
        reqType: ReqType.Get,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.ChildAllCatAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
