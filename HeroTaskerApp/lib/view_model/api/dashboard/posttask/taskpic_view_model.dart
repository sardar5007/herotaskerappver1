import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/DelPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

class TaskPicViewModel<T> with Mixin {
  Future<void> fetchDataGetPics({
    BuildContext context,
    int taskId,
    MediaUploadFilesModel mediaModel,
  }) async {
    try {
      final url =
          APIPostTaskCfg.GET_PIC_URL.replaceAll("#taskId#", taskId.toString());
      await NetworkMgr()
          .req<GetPicAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.GetPicAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> fetchDataSavePic({
    BuildContext context,
    int taskId,
    int mediaId,
    MediaUploadFilesModel mediaModel,
  }) async {
    try {
      final param = {
        "EntityId": taskId,
        "EntityName": "Task",
        "PropertyName": "Task",
        "Value": mediaId,
      };
      log(param);
      await NetworkMgr()
          .req<SavePicAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.SAVE_PIC_URL,
        param: param,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.SavePicAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> fetchDataDelPic({
    BuildContext context,
    int index, //  list index for removing
    int mediaId,
  }) async {
    try {
      await NetworkMgr()
          .req<DelPicAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.DEL_PIC_URL
            .replaceAll("#mediaId#", mediaId.toString()),
        reqType: ReqType.Delete,
      )
          .then((model) async {
        model.index = index;
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.DelPicAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
