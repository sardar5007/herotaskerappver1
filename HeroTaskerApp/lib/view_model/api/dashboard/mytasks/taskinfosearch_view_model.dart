import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class TaskInfoSearchViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    int page,
    int count,
    String searchText,
    double distance,
    double fromPrice,
    double toPrice,
    double lat,
    double lng,
    int status,
  }) async {
    try {
      await NetworkMgr().req<TaskInfoSearchAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": count,
          "Distance": distance ?? 500,
          "FromPrice": fromPrice ?? 20,
          "InPersonOrOnline": 0,
          "IsHideAssignTask": false,
          "Latitude": lat ?? 0.0,
          "Location": "",
          "Longitude": lng ?? 0.0,
          "Page": page,
          "SearchText": searchText ?? '',
          "Status": status,
          "ToPrice": toPrice ?? 100000,
          "UserId": userData.userModel.id,
        },
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.TaskInfoSearchAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
