import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/dashboard/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class UserRatingSummaryViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
  }) async {
    try {
      await NetworkMgr()
          .req<UserRatingSummaryAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.USER_RATING_SUMMARY_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(APIObserverState.STATE_DONE,
            APIType.UserRatingSummaryAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
