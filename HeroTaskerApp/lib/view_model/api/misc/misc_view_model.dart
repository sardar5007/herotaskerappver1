import 'dart:io';

import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/app_data/UserDevice.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/services.dart';
import 'package:mypkg/controller/Mixin.dart';

class MiscViewModel<T> with Mixin {
  Future<void> fetchDataFCMDeviceInfo({
    context,
  }) async {
    try {
      final param = await FcmDeviceInfoHelper().getParam();
      log(param);
      await NetworkMgr()
          .req<FcmDeviceInfoAPIModel, Null>(
        context: context,
        url: APIMiscCfg.FCM_DEVICE_INFO_URL,
        param: param,
        isLoading: false,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.FcmDeviceInfoAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> fetchDataUserDevices({
    context,
    eventType,
  }) async {
    try {
      String deviceId = "";
      String deviceName = "";
      String modelName = "";
      String deviceVersion = "";
      try {
        if (Platform.isAndroid) {
          final build = userDevice.androidInfo;
          deviceName = build.model;
          deviceId = build.androidId;
          modelName = build.model;
          deviceVersion = build.version.toString();
        } else if (Platform.isIOS) {
          final data = userDevice.iosInfo;
          deviceName = data.name;
          deviceId = data.identifierForVendor;
          deviceVersion = data.systemVersion;
          modelName = data.utsname.machine;
        }
      } on PlatformException {
        log('Failed to get platform version');
      }

      await NetworkMgr()
          .req<UserDeviceAPIModel, Null>(
        context: context,
        url: APIMiscCfg.USER_DEVICES_POST_URL,
        param: {
          "DeviceId": deviceId,
          "DeviceName": deviceName,
          "EventDate": DateTime.now().toString(),
          "EventType": eventType,
          "ModleName": modelName,
          "OSVersion": deviceVersion,
          "Remarks": "",
          "UserId": userData.userModel.id,
        },
        isLoading: false,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.UserDeviceAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
