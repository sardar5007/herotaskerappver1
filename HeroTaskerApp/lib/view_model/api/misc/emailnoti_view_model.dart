import 'dart:io';

import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/app_data/UserDevice.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/services.dart';
import 'package:mypkg/controller/Mixin.dart';

class EmailNotiViewModel<T> with Mixin {
  Future<void> fetchDataSendEmailNoti({
    context,
    int taskId,
  }) async {
    try {
      await NetworkMgr()
          .req<CommonAPIModel, Null>(
        context: context,
        url: APIPostTaskCfg.TASK_EMAI_NOTI_GET_URL
            .replaceAll("#taskId#", taskId.toString()),
        reqType: ReqType.Get,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.EmailNoti, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
