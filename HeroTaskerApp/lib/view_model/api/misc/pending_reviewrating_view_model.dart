import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/misc/rating/PendingReviewRatingByUserIdAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class PendingReviewRatingByUserIDViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
  }) async {
    try {
      final url = APIPostTaskCfg.PENDING_REVIEWRATING_BYUSERID_GET_URL
          .replaceAll("#userId#", userData.userModel.id.toString());
      await NetworkMgr()
          .req<PendingReviewRatingByUserIdAPIModel, Null>(
        context: context,
        url: url,
        isLoading: false,
        reqType: ReqType.Get,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(APIObserverState.STATE_DONE,
            APIType.PendingReviewRatingByUserIdAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
