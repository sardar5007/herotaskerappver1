import 'dart:io';
import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class MediaUploadFileViewModel<T> with Mixin {
  Future<void> fetchDataPicture({
    context,
    File file,
  }) async {
    try {
      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: APIMiscCfg.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(APIObserverState.STATE_DONE,
            APIType.MediaUploadFilesAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
