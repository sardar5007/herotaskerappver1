import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class LoginViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    email,
    pwd,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_URL,
        param: LoginHelper().getParam(email: email, pwd: pwd),
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.LoginAPIModel, model);
      });
    } catch (e) {
      log(e);
    }
  }
}
