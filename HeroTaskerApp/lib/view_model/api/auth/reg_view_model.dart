import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/auth/RegHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class RegViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    email,
    pwd,
    fname,
    lname,
    mobile,
    dob,
    gender,
    address,
  }) async {
    try {
      final param = RegHelper().getParam(
        email: email,
        pwd: pwd,
        fname: fname,
        lname: lname,
        fullName: fname + ' ' + lname,
        phone: mobile,
        dob: dob,
        gender: gender,
        address: address,
      );
      await NetworkMgr()
          .req<RegAPIModel, Null>(
        context: context,
        url: APIAuthCfg.REG_URL,
        param: param,
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.RegAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
