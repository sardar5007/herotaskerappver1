import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/auth/otp/LoginMobOtpPostHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class SMS2ViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    mobile,
    userId,
  }) async {
    try {
      await NetworkMgr()
          .req<MobileUserOtpPostAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_MOBILE_OTP_POST_URL,
        param: LoginMobOtpPostHelper().getParam(
          mobileNumber: mobile,
          userId: userId,
        ),
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(APIObserverState.STATE_DONE,
            APIType.MobileUserOtpPostAPIModel, model);
      });
    } catch (e) {}
  }
}
