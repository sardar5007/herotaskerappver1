import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/auth/otp/LoginMobOtpPutHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class SMS3ViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    otpCode,
    mobile,
    userId,
  }) async {
    try {
      await NetworkMgr()
          .req<MobileUserOtpPutAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_MOBILE_OTP_PUT_URL,
        reqType: ReqType.Put,
        param: LoginMobOtpPutHelper().getParam(
          mobileNumber: mobile,
          otpCode: otpCode,
          userId: userId,
        ),
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(APIObserverState.STATE_DONE,
            APIType.MobileUserOtpPutAPIModel, model);
      });
    } catch (e) {}
  }
}
