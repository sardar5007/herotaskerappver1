import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/auth/profile/RegProfileHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class RegProfileViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    address,
    briefBio = '',
    communityId,
    dob,
    email,
    fname,
    lname,
    cohort,
    headline = '',
    lat,
    lng,
    mobile,
  }) async {
    try {
      await NetworkMgr()
          .req<RegProfileAPIModel, Null>(
        context: context,
        url: APIAuthCfg.REG_PROFILE_PUT_URL,
        reqType: ReqType.Put,
        param: RegProfileHelper().getParam(
          address: address,
          briefBio: briefBio,
          communityId: communityId,
          dob: dob,
          email: email,
          fname: fname,
          cohort: cohort,
          headline: headline,
          lname: lname,
          lat: lat,
          lng: lng,
          mobile: mobile,
        ),
      )
          .then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.RegProfileAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
