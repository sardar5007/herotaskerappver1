import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:mypkg/controller/Mixin.dart';

class ForgotViewModel<T> with Mixin {
  Future<void> fetchData({
    context,
    email,
  }) async {
    try {
      await NetworkMgr().req<ForgotAPIModel, Null>(
        context: context,
        url: APIAuthCfg.FORGOT_URL,
        param: {'email': email},
      ).then((model) async {
        final APIStateProvider _stateProvider = new APIStateProvider();
        _stateProvider.notify(
            APIObserverState.STATE_DONE, APIType.ForgotAPIModel, model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
