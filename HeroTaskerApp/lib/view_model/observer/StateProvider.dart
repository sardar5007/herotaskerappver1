//Singleton reusable class
//  https://medium.com/@nshansundar/simple-observer-pattern-to-notify-changes-across-screens-in-flutter-dart-f035dbd990a9

abstract class StateListener {
  void onStateChanged(ObserverState state, dynamic data);
}

enum ObserverState {
  STATE_RELOAD_TAB,
  STATE_OPEN_HELP_DIALOG,
  STATE_HELP_DIALOG_TAB,
  STATE_LOGOUT,
}

class StateProvider {
  List<StateListener> observers = [];
  static final StateProvider _instance = new StateProvider.internal();
  factory StateProvider() => _instance;
  StateProvider.internal() {
    //observers = new List<StateListener>();
    initState();
  }
  void initState() async {}

  void subscribe(StateListener listener) {
    observers.add(listener);
  }

  void unsubscribe(StateListener listener) {
    observers.remove(listener);
  }

  void notify(dynamic state, dynamic data) {
    observers.forEach((StateListener obj) => obj.onStateChanged(state, data));
  }

  void dispose(StateListener thisObserver) {
    for (var obj in observers) {
      if (obj == thisObserver) {
        observers.remove(obj);
      }
    }
  }
}
