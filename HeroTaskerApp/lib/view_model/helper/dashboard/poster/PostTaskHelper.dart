class PostTaskHelper {
  static const List<Map<String, dynamic>> listTask = [
    {
      'isCat': false,
      'name': 'Cleaning',
      'title': 'Cleaning',
      'icon': 'assets/images/tasks/task_cleaning.svg',
    },
    {
      'isCat': false,
      'name': 'Anythings',
      'title': 'Anythings',
      'icon': 'assets/images/tasks/task_others.svg',
    },
    {
      'isCat': true,
      'name': 'All Categories',
      'title': 'All Categories',
      'icon': 'assets/images/tasks/task_allcat.svg',
    },
    {
      'isCat': false,
      'name': 'HandyMan',
      'title': 'HandyMan',
      'icon': 'assets/images/tasks/task_handyman.svg',
    },
    {
      'isCat': false,
      'name': 'Maid',
      'title': 'Maid',
      'icon': 'assets/images/tasks/task_maid.svg',
    },
    {
      'isCat': false,
      'name': 'Electrician',
      'title': 'Electrician',
      'icon': 'assets/images/tasks/task_electrician.svg',
    },
    {
      'isCat': false,
      'name': 'Part Time Job',
      'title': 'Part Time Job',
      'icon': 'assets/images/tasks/task_part_time_job.svg',
    },
    {
      'isCat': false,
      'name': 'Delivery',
      'title': 'Delivery',
      'icon': 'assets/images/tasks/task_delivery.svg',
    },
    {
      'isCat': false,
      'name': 'Computer/IT',
      'title': 'Computer/IT',
      'icon': 'assets/images/tasks/task_it.svg',
    },
    {
      'isCat': false,
      'name': 'Pest Control',
      'title': 'Pest Control',
      'icon': 'assets/images/tasks/task_pest.svg',
    },
    {
      'isCat': false,
      'name': 'Shifting',
      'title': 'Shifting/Moving',
      'icon': 'assets/images/tasks/task_moving.svg',
    },
    {
      'isCat': false,
      'name': 'Beauty Parlour',
      'title': 'Beauty Parlour',
      'icon': 'assets/images/tasks/task_beautician.svg',
    },
    {
      'isCat': false,
      'name': 'Tutor & Trainer',
      'title': 'Tutor & Trainer',
      'icon': 'assets/images/tasks/task_tutor.svg',
    },
    {
      'isCat': false,
      'name': 'Marketing',
      'title': 'Marketing',
      'icon': 'assets/images/tasks/task_marketting.svg',
    },
    {
      'isCat': false,
      'name': 'Tailor',
      'title': 'Tailor',
      'icon': 'assets/images/tasks/task_tailor.svg',
    },
    {
      'isCat': false,
      'name': 'Hire Driver',
      'title': 'Hire Driver',
      'icon': 'assets/images/tasks/task_driver.svg',
    },
    {
      'isCat': false,
      'name': 'Rent Car',
      'title': 'Rent Car',
      'icon': 'assets/images/tasks/task_rentcar.svg',
    },
    {
      'isCat': false,
      'name': '',
      'title': 'Rent Truck',
      'icon': 'assets/images/tasks/task_renttruck.svg',
    },
    {
      'isCat': false,
      'name': 'Security Gaurd',
      'title': 'Security Gaurd',
      'icon': 'assets/images/tasks/task_guard.svg',
    },
    {
      'isCat': false,
      'name': 'Admin',
      'title': 'Admin',
      'icon': 'assets/images/tasks/task_admin.svg',
    },
    {
      'isCat': false,
      'name': 'Shopping',
      'title': 'Shopping',
      'icon': 'assets/images/tasks/task_shopping.svg',
    },
    {
      'isCat': false,
      'name': 'Events',
      'title': 'Events',
      'icon': 'assets/images/tasks/task_events.svg',
    },
    {
      'isCat': false,
      'name': 'Laundry',
      'title': 'Laundry',
      'icon': 'assets/images/tasks/task_laundry.svg',
    },
  ];
}
