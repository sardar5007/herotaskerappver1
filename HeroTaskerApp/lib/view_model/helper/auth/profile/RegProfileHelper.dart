import 'package:aitl/data/app_data/UserData.dart';

class RegProfileHelper {
  getParam({
    String address,
    String briefBio,
    int communityId,
    String dob,
    String email,
    String fname,
    String lname,
    String cohort,
    String headline,
    double lat,
    double lng,
    String mobile,
  }) {
    return {
      "Address": address,
      "BriefBio": briefBio,
      "CommunityId": communityId.toString(),
      "DateofBirth": dob,
      "Email": email,
      "FirstName": fname,
      "Cohort": cohort,
      "Headline": headline,
      "Id": userData.userModel.id,
      "LastName": lname,
      "Latitude": lat.toString(),
      "Longitude": lng.toString(),
      "MobileNumber": mobile,
    };
  }
}
