import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:mypkg/controller/Mixin.dart';
import 'package:polygon_clipper/polygon_border.dart';

class ProfileHelper with Mixin {
  static const String ProfilePicFG_Key = "ProfilePicFG";
  static const String ProfilePicBG_Key = "ProfilePicBG";

  downloadProfileImages({BuildContext context, isLoading = true}) async {
    if (userData.userModel.profileImageUrl != '' &&
        userData.userModel.profileImageUrl.startsWith("http")) {
      await NetworkMgr().downloadFile(
          context: context,
          url: userData.userModel.profileImageUrl,
          pathName: ProfileHelper.ProfilePicFG_Key,
          isLoading: isLoading,
          callback: (path) {
            if (path != null) {
              PrefMgr.shared.setPrefStr(ProfileHelper.ProfilePicFG_Key, path);
            }
          });
    }
    if (userData.userModel.coverImageUrl != '' &&
        userData.userModel.coverImageUrl.startsWith("http")) {
      await NetworkMgr().downloadFile(
          url: userData.userModel.coverImageUrl,
          pathName: ProfileHelper.ProfilePicBG_Key,
          callback: (path) {
            if (path != null) {
              PrefMgr.shared.setPrefStr(ProfileHelper.ProfilePicBG_Key, path);
            }
          });
    }
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({int rate, int reviews}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getStarsRow(rate, MyTheme.redColor),
          SizedBox(height: 10),
          Txt(
              txt: "(" + reviews.toString() + " Reviews)",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .7,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getCompletionText({int pa}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/icons/ribbon_icon.png",
            width: 20,
            height: 20,
          ),
          Txt(
              txt: pa.toString() + "% Completion Rate",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .7,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getAvatorStarView({int rate}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(18.0),
            margin: EdgeInsets.only(right: 16.0),
            decoration: ShapeDecoration(
                shape: PolygonBorder(
                    sides: 6,
                    borderRadius: 1,
                    border: BorderSide(color: Colors.grey, width: 0.5))),
            child: Image.asset(
              "assets/images/icons/user_icon.png",
              width: 40,
              height: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getStarsRow(rate, Colors.cyan),
              SizedBox(height: 10),
              Txt(
                  txt: getTimeAgoTxt("2021-01-27T16:16:33.47Z"),
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          )
        ],
      ),
    );
  }
}
