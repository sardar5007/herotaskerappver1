import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class AuthHelper {
  drawAuthHeaderImage(context) {
    return Container(
      //width: getWP(context, 70),
      //height: getHP(context, 30),
      child: Column(
        children: [
          SvgPicture.asset(
            'assets/images/screens/welcome/welcome_theme.svg',
            //fit: BoxFit.fill,
          ),
          RichText(
            text: new TextSpan(
              // Note: Styles for TextSpans must be explicitly defined.
              // Child text spans will inherit styles from parent
              style: new TextStyle(
                fontSize: ResponsiveFlutter.of(context)
                    .fontSize(MyTheme.txtSize + .5),
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
              children: <TextSpan>[
                new TextSpan(
                  text: 'Bring ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize + .5)),
                ),
                new TextSpan(
                  text: 'HEROS',
                  style: TextStyle(
                      color: MyTheme.redColor,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize + .5)),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15, left: 20, right: 20, bottom: 20),
            child: Txt(
              txt: "to do your to-dos",
              txtColor: MyTheme.timelineTitleColor,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.center,
              isBold: true,
              //txtLineSpace: 1.5,
            ),
          ),
        ],
      ),
    );
  }

  drawGoogleFBLoginButtons(
      {BuildContext context, String icon, double height, Function callback}) {
    return Expanded(
      //flex: 2,
      child: Container(
        height: height,
        child: OutlinedButton(
          onPressed: () {
            callback();
          },
          style: ButtonStyle(
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
            ),
            side: MaterialStateProperty.resolveWith((states) {
              return BorderSide(color: Colors.black, width: 1.5);
            }),
          ),
          child: SvgPicture.asset(
            'assets/images/icons/svg/' + icon + '.svg',
            //fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
