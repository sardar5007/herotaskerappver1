class LoginMobOtpPutHelper {
  getParam({
    String mobileNumber,
    String otpCode = '',
    int userId,
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "Status": 101,
      "UserId": userId,
    };
  }
}
