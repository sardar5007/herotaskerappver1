import 'dart:io';

class LoginRegOtpFBHelper {
  getParam({
    String mobileNumber,
    String otpCode,
    int userId,
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
      "Persist": true,
    };
  }
}
