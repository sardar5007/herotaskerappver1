import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';

class SendOtpNotiHelper {
  getUrl({int otpId}) {
    var url = APIAuthCfg.SEND_OTP_NOTI_URL;
    url = url.replaceAll("#otpId#", otpId.toString());
    return url;
  }
}
