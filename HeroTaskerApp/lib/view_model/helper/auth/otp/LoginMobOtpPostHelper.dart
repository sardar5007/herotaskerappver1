class LoginMobOtpPostHelper {
  getParam({
    String mobileNumber,
    String countryCode = '880',
    String status = '101',
    String otpCode = '',
    int userId,
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": "",
      "Status": 101,
      "UserId": userId,
    };
  }
}
